#Used
##PIC
[osdev](http://wiki.osdev.org/8259_PIC)
[algortman.de](https://www.youtube.com/watch?v=XGSOl1QZVKI&t=45s)
[osdever](http://www.osdever.net/tutorials/view/programming-the-pic)

##Testing
[Testing in software](http://www.testingeducation.org/BBST/testdesign/KanerBachPettichord_Lessons_Learned_in_SW_testingCh3-1.pdf)
[art of software testing](https://books.google.co.uk/books?hl=en&lr=&id=GjyEFPkMCwcC&oi=fnd&pg=PT5&dq=software+testing&ots=AgwSH0oW_e&sig=fbuGEkZ_gqtALipg-7_pQB15wm4#v=onepage&q=software%20testing&f=false)
[Software testing techniques - uclan](http://eds.b.ebscohost.com/eds/detail/detail?vid=12&sid=07d537ee-412b-4145-90d2-99d919c1903d%40sessionmgr107&hid=121&bdata=JnNpdGU9ZWRzLWxpdmU%3d#AN=uclan.865338&db=cat00075a)

[Software testing fundamentals - uclan](http://eds.b.ebscohost.com/eds/detail/detail?sid=07d537ee-412b-4145-90d2-99d919c1903d%40sessionmgr107&vid=13&hid=121&bdata=JnNpdGU9ZWRzLWxpdmU%3d#AN=uclan.947966&db=cat00075a)
##Languages
###Assembly
[Assembly language - uclan - collage](http://eds.b.ebscohost.com/eds/detail/detail?vid=18&sid=07d537ee-412b-4145-90d2-99d919c1903d%40sessionmgr107&hid=121&bdata=JnNpdGU9ZWRzLWxpdmU%3d#AN=uclan.467094&db=cat00075a)

###linker
[LD Scripts](ftp://ftp.gnu.org/old-gnu/Manuals/ld-2.9.1/html_mono/ld.html#SEC6)

##dissasemblers
[www.onlinedisassembler.com](www.onlinedisassembler.com)

##Cross compiler

[Osdev.net](http://wiki.osdev.org/GCC_Cross-Compiler)
[waybackmachine copy-2016_05_15](https://web.archive.org/web/20160515214350/http://wiki.osdev.org/GCC_Cross-Compiler)
##others

[Academic Intel.co.uk](http://www.intel.co.uk/content/www/uk/en/processors/architectures-software-developer-manuals.html)
[Youtube.com (prof/dr/assistant Nick Blundel)](https://www.youtube.com/watch?v=YvZhgRO7hL4)
[Semi-Academic University of Birmingham (prof/dr/assistant Nick Blundel)](http://www.cs.bham.ac.uk/~exr/lectures/opsys/10_11/lectures/os-dev.pdf)
[github SamyPesse pdf](https://github.com/SamyPesse/How-to-Make-a-Computer-Operating-System)
[Qemu instruction reference](http://linux.die.net/man/1/qemu-kvm)
[Academic Qemu manual](http://wiki.qemu.org/download/qemu-doc.html)
[Osdev](http://wiki.osdev.org/Main_Page)
[Mikeos](http://mikeos.sourceforge.net/)
[Grub legacy]()
[Example unix like os](https://github.com/aplabs/bsos)
[Introduction to assembly](http://www.tutorialspoint.com/assembly_programming/)


#Not used but look intersting
##make files
[The Linux kernel: a case study of build system variability - uclan](http://eds.b.ebscohost.com/eds/detail/detail?vid=33&sid=07d537ee-412b-4145-90d2-99d919c1903d%40sessionmgr107&hid=121&bdata=JnNpdGU9ZWRzLWxpdmU%3d#AN=14529399&db=inh)
[The kernel configuration and build process-uclan](The kernel configuration and build process)
##Languages
###C
[C : a practical introduction-uclan](http://eds.b.ebscohost.com/eds/detail/detail?vid=15&sid=07d537ee-412b-4145-90d2-99d919c1903d%40sessionmgr107&hid=121&bdata=JnNpdGU9ZWRzLWxpdmU%3d#AN=uclan.309350&db=cat00075a)

##Kernel dev
[linux kernal development](https://books.google.co.uk/books?hl=en&lr=&id=3MWRMYRwulIC&oi=fnd&pg=PR7&dq=kernel+development&ots=2aoGo9t7nk&sig=KNYqygBqqj-JoS5ifOsMg8gG6Ng#v=onepage&q=kernel%20development&f=false)

[OS X and iOS Kernel Programming - uclan](https://capitadiscovery.co.uk/uclan/items/1014148?resultsUri=https%3A%2F%2Fcapitadiscovery.co.uk%2Fuclan%2Fitems%3Fquery%3Dos%2Bdevelopment%26max%3D5%26offset%3D15%26aj%3Dt#availability)

[Control-based Operating System Design - uclan](https://capitadiscovery.co.uk/uclan/items/dda-10/PBCE089E?resultsUri=https%3A%2F%2Fcapitadiscovery.co.uk%2Fuclan%2Fitems%3Fquery%3Dkernel%2Bdevelopment%26max%3D5#availability)

[Operating system concepts - uclan](http://eds.b.ebscohost.com/eds/detail/detail?vid=2&sid=07d537ee-412b-4145-90d2-99d919c1903d%40sessionmgr107&hid=121&bdata=JnNpdGU9ZWRzLWxpdmU%3d#AN=uclan.1083346&db=cat00075a)


##os development
[• S. Baskiyar, and N. Meghanathan “A Survey of Contemporary Real-time
Operating Systems” Informatica (2005).](#)
[Herman Hartig,Michael Hohmuth, Jochen Liedtke, JeanWolter, and Sebastian
Schonberg “The Performance of u-kernel based Systems”, Proceedings
of the 16th ACM Symposium on Operating Systems Principles (1997).](#)
[John A. Stankovic, and Krithi Ramamritham “The Spring Kernel: A New
Paradigm for Real-Time Operating Systems”, ACM SIGOPS Operating
Systems Review (1989).](#)
[Butler W. Lampson “Hints for Computer System Design”, Proceedings of
the 9th ACM Symposium on Operating Systems Principles (1983).](#)
[Os concepts](http://codex.cs.yale.edu/avi/os-book/OS9/)
[Recent papers 2014](http://www.engpaper.net/operating-system-research-papers-2012.htm)


##Testing
[Testing in software](http://www.testingeducation.org/BBST/testdesign/KanerBachPettichord_Lessons_Learned_in_SW_testingCh3-1.pdf)
[art of software testing](https://books.google.co.uk/books?hl=en&lr=&id=GjyEFPkMCwcC&oi=fnd&pg=PT5&dq=software+testing&ots=AgwSH0oW_e&sig=fbuGEkZ_gqtALipg-7_pQB15wm4#v=onepage&q=software%20testing&f=false)
[Software testing techniques - uclan](http://eds.b.ebscohost.com/eds/detail/detail?vid=12&sid=07d537ee-412b-4145-90d2-99d919c1903d%40sessionmgr107&hid=121&bdata=JnNpdGU9ZWRzLWxpdmU%3d#AN=uclan.865338&db=cat00075a)

[Software testing fundamentals - uclan](http://eds.b.ebscohost.com/eds/detail/detail?sid=07d537ee-412b-4145-90d2-99d919c1903d%40sessionmgr107&vid=13&hid=121&bdata=JnNpdGU9ZWRzLWxpdmU%3d#AN=uclan.947966&db=cat00075a)

