#!/usr/bin/python
import os
import sys

outFile = sys.argv[1]
#Loop through each pram
for i in range(2,len(sys.argv)):
	#If first param wipe file
	if(i==2):
		os.system("cat " + sys.argv[i] + " > " + outFile)
	#else append to end of the file
	else:
		os.system("cat " + sys.argv[i] + " >> " + outFile)
	#Read current size of file
	size = os.path.getsize(outFile)
	#Calculate sector pad size
	remainder = 512 - (size % 512)
	#Pad to end of sector
	if(remainder<512):
		os.system("dd if=/dev/zero bs=1 count=" + `remainder` + ">>" + outFile)
#Pad the image to 256 sectors
size = os.path.getsize(outFile)
sectorCountToWrite = 200 # - (size//512)
os.system("dd if=/dev/zero bs=512 count="+ `sectorCountToWrite` +">>" + outFile)
