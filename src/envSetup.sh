#this file runs as sudo

#Install which ever version of qemu is avilible on the distro
apt install qemu -y || apt install qemu-system-x86 -y
#install bochs
apt install bochs bochs-x -y

#install python which is used in the make files
apt install python -y

#install the assembler
apt install nasm -y

#install the distro gcc used to make the version of GCC which inturn makes the Operating Sytem
apt install build-essential -y
#install wget which is used to get the source of GCC
apt install wget -y
#install libraries required to compile GCC
apt install flex bison texinfo libmpc-dev -y

