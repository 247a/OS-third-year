#Make a directory for the source downloaded for GCC which will not be put into git
mkdir crossCompilerSrc
#move to the source dirctory
cd crossCompilerSrc
#get and extract the GCC source
wget ftp://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/releases/gcc-6.2.0/gcc-6.2.0.tar.bz2
tar -jxvf gcc-6.2.0.tar.bz2
#Run the GCC setup script which downloads things like the math lib
gcc-6.2.0/contrib/download_prerequisites

#Add where to save the compiled compiler to our environment variables
export PREFIX="$HOME/opt/cross"
#Also add the fact that we are makeing a version to run on an x86 computer
export TARGET=i686-elf
#This would be if we wanted to add to our execution path, due to how i access the GCC and the fact it could collide with the distro installed version i don't add to path
#export PATH="$PREFIX/bin:$PATH"

#Get and extract binutils which are a collection of applications which GCC uses like the linker and objcopy as well as this operating systems make files
wget http://ftp.gnu.org/gnu/binutils/binutils-2.27.tar.gz
tar -zxvf binutils-2.27.tar.gz
#Make a directory to output the compiled object files of bin utils and enter it
mkdir build-binutils
cd build-binutils
#Compile and install to this output directory
../binutils-2.27/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make
make install
#exit the binutils object output directory
cd ..
#Check that Binutils was successfully installed
which -- $TARGET-as || echo $TARGET-as is not in the PATH
#make and enter the directory to place the object files for GCC
mkdir build-gcc
cd build-gcc
#Configure and build the cross compiler with only c and c++ support
../gcc-6.2.0/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
make all-gcc
make all-target-libgcc
make install-gcc
make install-target-libgcc
#exit the GCC object out put dir
cd ..
#exit the crossCompilerSrc
cd ..
#Clean up the source directory by removing the GCC source as it is not required after installation
rm -R crossCompilerSrc --force

