;[org 0x7c00]		;tell the assembler that the load address is 0x7C00h so labels point to the correct location
[bits 16]			;state the code to be compiled in 16 bit mode
[global _start]
_start:
; ====================================================================
Stage1:				; Stage 1 bootloader memory starts
					; Sets up real mode util methods
					; Moves to protected mode
					; Sets up MBR
					; Also loads Stage 2
; ====================================================================
xchg bx, bx ;bochs magic break point
	mov [bootdev], dl
	mov ax, 7000	; Set up 4K of stack space above buffer
				; 8k buffer = 512 paragraphs + 32 paragraphs (loader)
	cli					; Disable interrupts while changing stack ;!!!
	;SS = 0x7000
	;SP = fff0
	;SS:SP = 0x7FFF0
	mov ss, ax
	mov sp, 0xfff0
	sti					; Restore interrupts
	;NOTE: A few BIOSes improperly set DL
	cmp dl, 0
	je .start_driveOk
	call hdd_diskProperties
.start_driveOk:
mov al, 't'
	
	mov bl, 50				; Read 40 sectors (Kernel and Elf)
	mov ax, 1				; Root dir starts at logical sector 1
	mov si, kernel			; Set ES:BX to point to our buffer (see end of code)
	mov di, 0
	call hdd_ReadDrive

;switch to protected mode and move to stage 2
	mov di, kernel		;Load return address for protected mode code(32bit)
	call modeSwitch_realToProtected ;compleate mode switch and jmp to kernel
	
	jmp hang;			; Should not hit so hang as a safty net

%include 'includes/includes.asm'
;Add ref to stage 2 here so that this stage can be self contained
kernel:
