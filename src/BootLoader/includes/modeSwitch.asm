[bits 16]	;sanity check that code compiles in 16bit
;------------------------------------------------------------
;			Real to Protected
;------------------------------------------------------------
;Switches the CPU into protected mode(32-bit) from real mode(16-bit)
;------------------------------------------------------------
;Inputs:
;	Registers:
;		DI:Return Address
;------------------------------------------------------------
;Returns:
;	N/A	
;-------------------------------------------------------------
modeSwitch_realToProtected:
  cli		;Clear interupts
  
  lgdt [GDT_Descriptor]	;Load the global descriptor table
  
  mov eax, cr0	;get cr0 register
  or eax, 0x1	;set last bit aka protected mode flag
  mov cr0,eax	;set cr0 register
  
  ;long jump to finish switch to protected
  ;and clear buffer
  
  jmp GDT_Code_Segment:private_mode_start
[bits 32]
private_mode_start:
  ;set registers to new valid value
  mov ax, GDT_Data_Segment ;load const to ax ready to put in segment registers
  mov ds, ax
  mov ss, ax
  mov es, ax
  mov fs, ax
  mov gs, ax
  ;Create new stack
  mov ebp, 0x90000 	;new base pointer
  mov esp,ebp 		;make stack empty
  
  jmp di;
  
[bits 16]	;make code following this include file
