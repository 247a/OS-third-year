%include 'includes/screen.asm'  	;Print to screen(includes code for debug)
%include 'includes/hdd.asm'		;Disk I/O functions
%include 'includes/hang.asm'		;infinite loop
%include 'includes/strings.asm'		;String constants
%include 'includes/GDT.asm'			;gdt table
%include 'includes/modeSwitch.asm'		;functions for switching between protected and real mode

;make sure this include is last in sector
%include 'includes/mbr.asm'		;Bootsig and mbr

