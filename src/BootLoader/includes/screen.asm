[global screen_printStringToScreen]
[global screen_printRegToScreen]

;------------------------------------------------------------
;				Print Text to Screen
;------------------------------------------------------------
;Uses the bios to write text to the screen in a teletype form
;------------------------------------------------------------
;Inputs:
;	Stack:
;		char to output
;------------------------------------------------------------
;Returns:
;	none
;-------------------------------------------------------------
;Example usage:
;	mov al, 't'
;	call screen_printToScreen
;Output:
;t
;-------------------------------------------------------------
screen_printCharToScreen:
	mov ah, 0x0e	;Bios function id
	int 0x10		;Call bios function
	ret				;return
;------------------------------------------------------------
;	Print register as hex to Screen(used for debug only)
;------------------------------------------------------------
;Prints the contents of register as hex
;------------------------------------------------------------
;Inputs:
;	Registers:
;		dx - Data to output
;------------------------------------------------------------
;Returns:
;	none
;-------------------------------------------------------------
;Example usage:
;	mov dx, 0x232e
;	call screen_printRegToScreen
;Output:
;0x0000
;-------------------------------------------------------------
screen_printRegToScreen:
	pusha							;Back up registers
	mov bx, strings_hex_template+5	;Setup loop counter and pointer
	.loop:
		mov al,dl					;move current active section of input
		and al,0x0F					;mask for current letter
		call .convert	;convert hex(ish) number to ascii value
		mov [bx],al								;write ascii value to memory in the hex template
		shr dx,4								;move to the next active section of input
		dec bx									;reduce the pointer and loop counter by 1
		cmp bx, strings_hex_template+1 ;if past last char exit loop
	jg .loop
	mov si,strings_hex_template			;set the hex template
	call screen_printStringToScreen		
	popa								;restore backed up registers
	ret									;return

.convert:
	cmp al, 0xA ;check if less than 0xa
	jl .num
		add al, 7 ;the correctional value for A-F
	.num:
	add al,48 ;shift to numbers
	ret
	
;------------------------------------------------------------
;				Print string to screen
;------------------------------------------------------------
;Uses bios and a null terminated string to print to screen
;------------------------------------------------------------
;Inputs:
;	Register:
;		si:
;			Contains pointer to null terminated string	
;------------------------------------------------------------
;Returns:
;	none
;------------------------------------------------------------
;Example usage:
;	string db 'hi',0
;	mov si, string
;	call screen_printStringToScreen
;Output:
;hi
;-------------------------------------------------------------
screen_printStringToScreen:
		pusha
		mov ah, 0x0e	;bios teletype
	.loop:
		lodsb			;load char form si into al and move to next char(more optimised than mov and inc or mov and add 1)
		cmp al, 0		;check if char is the end char
		je .end ;if end of string exit function
		int 0x10		;call bios
		jmp .loop;move to next char
	.end:
		popa
		ret