;---------------------------------------
;Segment pointers/indexs
;---------------------------------------
GDT_Code_Segment: equ GDT_Ring_0_Code_Seg - GDT_Start
GDT_Data_Segment: equ GDT_Ring_0_Data_Seg - GDT_Start


;-----------------------------------------
;	Global Descriptor Table
;------------------------------------------
;Table defining memory spits for code and 
;	data segments this can not be altered
;	easily at runtime
;Each Segment is 8 bytes in length
;------------------------------------------
GDT_Start:
  ;Entry 0 equals zero
  GDT_Null_Seg:
    dd 0
    dd 0
  ;--------------------------------------------------
  ;Entry 1 : code segment filling all avilible memory
  ;--------------------------------------------------
  ;Base Address (32bits): 0x0
  ;Segment size (20 bits): 0xfffff
  ;Present (1 bit) : 1(true) (allways true for any segment not null)
  ;Ring (2 bits) : 00b(ring 0)
  ;Descriptor type(1 bit): 1(Application)
  ;	(a 0 would mean system)
  ;Executable(1 bit) : 1(true)
  ;Conforming(Access Control) (1 bit) : 0 (Only Access able from current ring)
  ;	(if 1 can be accessed from current ring or lower)
  ;Readable(1 bit) : 1(true)
  ;	(never writeable for code segments)
  ;Accessed(1 bit) : 0(false)
  ;	(Cpu sets this flag to true when accessed)
  ;
  ;Granularity(Limit addressing size)(1 bit): 1(4 KiB blocks)
  ;	(if 0 then in byte blocks)
  ;Size (1 bit):1 (32bit seg)
  ;	(if 0 16bit seg)
  ;64bitSeg (1 bit):0 (false)
  ;avilible for system (1 bit):0(Unused)
  ;--------------------------------------------
  GDT_Ring_0_Code_Seg:
    dw 0xffff ;first 2 bytes of size
    dw 0x0    ;first 2 bytes of base address
    db 0x0    ;third byte of base address
    ;Present, Ring, Descriptor type
    ;Executable, Conforming, Readable, Accessed
    db 10011010b
    ;Granularity, Size, 64bitSeg, avilible for system
    ;Last 4 bits of size
    db 11001111b
    db 0x0    ;fourth and final byte of base address
  ;--------------------------------------------------
  ;Entry 2 : Data segment filling all avilible memory
  ;--------------------------------------------------
  ;Base Address (32bits): 0x0
  ;Segment size (20 bits): 0xfffff
  ;Present (1 bit) : 1(true) (allways true for any segment not null)
  ;Ring (2 bits) : 00b(ring 0)
  ;Descriptor type(1 bit): 1(Application)
  ;	(a 0 would mean system)
  ;Executable(1 bit) : 0(false)
  ;Conforming(Access Control) (1 bit) : 0 (Only Access able from current ring)
  ;	(if 1 can be accessed from current ring or lower)
  ;Writeable(1 bit) : 1(true)
  ;	(Always Readable for Data segments)
  ;Accessed(1 bit) : 0(false)
  ;	(Cpu sets this flag to true when accessed)
  ;
  ;Granularity(Limit addressing size)(1 bit): 1(4 KiB blocks)
  ;	(if 0 then in byte blocks)
  ;Size (1 bit):1 (32bit seg)
  ;	(if 0 16bit seg)
  ;64bitSeg (1 bit):0 (false)
  ;avilible for system (1 bit):0(Unused)
  ;--------------------------------------------
  GDT_Ring_0_Data_Seg:
    dw 0xffff ;first 2 bytes of end address
    dw 0x0    ;first 2 bytes of base address
    db 0x0    ;third byte of base address
    ;Present, Ring, Descriptor type
    ;Executable, Conforming, Writeable, Accessed
    db 10010010b
    ;Granularity, Size, 64bitSeg, avilible for system
    ;Last 4 bits of size
    db 11001111b
    db 0x0    ;fourth and final byte of base address  
;------------------------------------------
;Label indicating end of gdt
; for gdt length args
;------------------------------------------
GDT_End:




;----------------------------------------
;Global descriptor table descriptor
;----------------------------------------
; Size of GDT (Size = End - Start)
; Start of GDT
;----------------------------------------
GDT_Descriptor:
  dw GDT_End - GDT_Start -1 ;Size = End - Start
			    ;minus one for off by one error		    
  dd GDT_Start
 
 

