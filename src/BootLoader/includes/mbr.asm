[global MBR_Partition_Table]
[global MBR_BootSig]

mbr_const_codeSize equ 512-76 			;size of max code of bootsector
times mbr_const_codeSize-($-$$) db 0    ;Pad bootsector

;mbr
MBR_optionalUniqueDiskID times 10 db 0 ;ten bytes
MBR_Partition_Table:
	MBR_Partition1:
	%include 'includes/PartitionTable/Partition1.asm'
	MBR_Partition2:
	%include 'includes/PartitionTable/Partition2.asm'
	MBR_Partition3:
	%include 'includes/PartitionTable/Partition3.asm'
	MBR_Partition4:
	%include 'includes/PartitionTable/Partition4.asm'	
MBR_BootSig dw 0xAA55				; Magic number for bootable drive