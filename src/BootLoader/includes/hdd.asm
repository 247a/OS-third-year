[global hdd_ReadDrive]
;------------------------------------------------------------
;				Output disk error
;------------------------------------------------------------
;Shows to the user that a disk read critically failed 
;------------------------------------------------------------
;Inputs:
;	Registers:
;		CF, AH, AL - from hdd_readDisk on fail
;------------------------------------------------------------
;Returns:
;	An output to screen showing read failed	
;-------------------------------------------------------------
hdd_error:
	mov si,strings_hddLoad_unknown	;load error string
	call screen_printStringToScreen	;print error message
	jmp hang						;hang

;------------------------------------------------------------
;				Drive properties
;------------------------------------------------------------
;Gets the properties of the selected drive
;------------------------------------------------------------
;Inputs:
;	Registers:
;		DL - Drive device number
;------------------------------------------------------------
;Returns:
;	RAM:
;		SectorsPerTrack - number of sectors per track
;		Heads			- The number of heads on drive	
;-------------------------------------------------------------	
;Todo validate cx code
;-------------------------------------------------------------
hdd_diskProperties:
	mov ah, 8					; Get drive parameters
	int 13h						; I/O functions
	jc hdd_error				; If error return error and hang
	;convert Bios out put to usable data
	and cx, 3Fh					; Mask to max sector number
	mov [SectorsPerTrack], cx	; Save the number of sectors per track/cylinder
	movzx dx, dh				; Maximum head number
	add dx, 1					; Head numbers start at 0 - add 1 for total
	mov [Heads], dx				;save number of heads/sides
	ret
	

	
	
;------------------------------------------------------------
;				Read from drive
;------------------------------------------------------------
;Reads the contents of the drive at the selected sector at the memory
;location chosen
;------------------------------------------------------------
;Inputs:
;	Registers:
;		AX - Source sector
;		DI - Output Segment
;		SI - Output address
;		BL - number of sectors to read
;------------------------------------------------------------
;Returns:
;The contents of the drive at the selected sectors at the memory
;location chosen
;-------------------------------------------------------------
hdd_ReadDrive:	
	call .hdd_l2hts

	
	mov ah, 2			; Params for int 13h: read floppy sectors
	mov al, bl			; And read 1 sector
	
	
	mov bx, di			;can't set es directly
	mov es, bx
	mov bx, si

	pusha				; Prepare to enter loop by backing up all registers
.loop:
	popa				; In case registers are altered by int 13h e.g. status codes etc
	pusha				;return the backup (as pop removes the bakup)

	stc					; Some BIOSes don't set carry flag on error
	int 13h				; I/O functions
	
	jnc .end		; If read went OK do not retry
	;jmp hdd_error
	call .hdd_resetDrive			; else reset floppy controller and retry

	jnc .loop		; If floppy reset works retry read
	jmp hdd_error				; else error and hang

.end:
	
	popa				;clean the stack of the back up of registers
	ret
	
	
	
	
;------------------------------------------------------------
;		Calculate head, track and sector settings
;------------------------------------------------------------
;Reads the contents of the drive at the selected sector at the memory
;location chosen
;------------------------------------------------------------
;Inputs:
;	Registers:
;		AX - Source sector(zero based)
;------------------------------------------------------------
;Returns:
;	Registers:
;		correct registers for int 13h
;-------------------------------------------------------------
;Taken from mikeOS 
;(not endorsed)
;Licence available in /doc/mikelicence.txt
;-------------------------------------------------------------
;Todo remove need of this code
;-------------------------------------------------------------	
.hdd_l2hts:	
	;
	push bx
	push ax

	;cl = SectorsPerTrack++;
	call .loadSectorsPerTrackDivisor
	add dl, 01h					; Physical sectors start at 1
	mov cl, dl					; Sectors belong in CL for int 13h
	
	;dh = (byte) (SectorsPerTrack % Heads) ;% = modulo
	;dl = byte [bootdev]
	;ch = (byte) (SectorsPerTrack / Heads)
	call .loadSectorsPerTrackDivisor
	xor dx,dx					; dispose of remainder
	div word [Heads]	
	mov dh, dl					; Head
	mov dl, byte [bootdev]		; Set correct device
	mov ch, al					; Track

	pop ax
	pop bx

	ret
.loadSectorsPerTrackDivisor:
	mov bx, ax			; Save logical sector
	xor dx, dx
	div word [SectorsPerTrack]
	ret
;------------------------------------------------------------
;				Reset drive
;------------------------------------------------------------
;Resets the drive so a new operation can ocoure
;------------------------------------------------------------
;Inputs:
;	RAM:
;		bootdev - disk to reset
;------------------------------------------------------------
;Returns:
;	Registers:
;		CF - carry flag
;-------------------------------------------------------------
.hdd_resetDrive:	
	push ax					;save used vars
	push dx
	xor ax,ax 				;ax 0 = Reset drive
	mov dl, byte [bootdev]	;DL   = Drive to reset
	stc						;Some Bios's don't set carry flag but do clear it
	int 13h 				;I/O functions
	pop dx					;restore used vars
	pop ax
	ret
	
