#!/usr/bin/python
import os
import sys
import struct

Kernel=sys.argv[1]
Kernelsize = os.path.getsize(Kernel)/512
if(Kernelsize%512>0):
	Kernelsize +=1 #if kernel is not whole 512 multiple ajust by one

ELf = sys.argv[2]
Elfsize = os.path.getsize(ELf)/512
Elfstart= Kernelsize+1#account for bootloader
if(Elfstart%512>0):
	Elfsize +=1 #if elf is not whole 512 multiple ajust by one
	
def Part1():
	FileContent = [
		"db 0x80",#make partision active(main)
		"db 0,0,0",#CHS start null aka invalid so lba takes over
		"db 0x8a",#Linux Kernel Partition used by AiR-BOOT https://www.win.tue.nl/~aeb/partitions/partition_types-1.html
		"db 0,0,0",#CHS end null aka invalid so lba takes over
		"dd 1",#fat starts at the second logical sector
		"dd " + `Kernelsize`#Total Sectors
	]
	print("Kernelsize:" + `Kernelsize`)
	with open(sys.argv[3]+ "1.asm",'w') as outfile:
		outfile.writelines("%s\n" % item for item in FileContent)
def Part2():
	FileContent = [
		"db 0",#make partision not active
		"db 0,0,0",#CHS start null aka invalid so lba takes over
		"db 0x20",#Unused[https://www.win.tue.nl/~aeb/partitions/partition_types-1.html]
		"db 0,0,0",#CHS end null aka invalid so lba takes over
		"dd " + `Elfstart`,#fat starts at the second logical sector
		"dd " + `Elfsize`#Total Sectors
	]
	print("Elfsize:" + `Elfsize`)
	print("Elfstart:" + `Elfstart`)
	with open(sys.argv[3]+ "2.asm",'w') as outfile:
		outfile.writelines("%s\n" % item for item in FileContent)
def Part3():
	FileContent = [
		"db 0",#make partision not active
		"db 0,0,0",#CHS start null aka invalid so lba takes over
		"db 0",#empty
		"db 0,0,0",#CHS end null aka invalid so lba takes over
		"dd 0",#fat starts at the second logical sector
		"dd 0"#Total Sectors
	]
	with open(sys.argv[3]+ "3.asm",'w') as outfile:
		outfile.writelines("%s\n" % item for item in FileContent)
def Part4():
	FileContent = [
		"db 0",#make partision not active
		"db 0,0,0",#CHS start null aka invalid so lba takes over
		"db 0",#empty
		"db 0,0,0",#CHS end null aka invalid so lba takes over
		"dd 0",#fat starts at the second logical sector
		"dd 0"#Total Sectors
	]
	with open(sys.argv[3]+ "4.asm",'w') as outfile:
		outfile.writelines("%s\n" % item for item in FileContent)
def MBR():
	Part1()
	Part2()
	Part3()
	Part4()
MBR()

