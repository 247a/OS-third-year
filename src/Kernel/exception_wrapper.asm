;;;
;;;this was made with guidence from http://www.osdever.net/bkerndev/Docs/isrs.htm
;;;

[global divide_error]
[global debug]
[global nmi]
[global int3]
[global overflow]
[global bounds]
[global invalid_op]
[global device_not_available]
[global double_fault]
[global coprocessor_segment_overrun]
[global invalid_TSS]
[global segment_not_present]
[global stack_segment]
[global general_protection]
[global page_fault]
[global unknown_interupt]
[global coprocessor_error]
[global alignment_check]
[global machine_check]
[global simd_coprocessor_error]
[global reserved]
[global irq0]
[global irq1]
[global irq2]
[global irq3]
[global irq4]
[global irq5]
[global irq6]
[global irq7]
[global irq8]
[global irq9]
[global irq10]
[global irq11]
[global irq12]
[global irq13]
[global irq14]
[global irq15]
[global test_sys_call]
[extern exception_handler]
divide_error:
	
	push	0;Pad to make stack aligned due to no error on stack
	push	0;Error index in IDT
	jmp interuptServiceRoutine;
debug:
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	1;Error index in IDT
	jmp interuptServiceRoutine;
nmi:
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	2;Error index in IDT
	jmp interuptServiceRoutine;
int3:
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	3;Error index in IDT
	jmp interuptServiceRoutine;
overflow:
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	4;Error index in IDT
	jmp interuptServiceRoutine;
 bounds:
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	5;Error index in IDT
	jmp interuptServiceRoutine;
invalid_op: 
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	6;Error index in IDT
	jmp interuptServiceRoutine;
device_not_available:
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	7;Error index in IDT
	jmp interuptServiceRoutine;
double_fault:
	 
	;don't pad as cpu pushes error code
	push	8;Error index in IDT
	jmp interuptServiceRoutine;
coprocessor_segment_overrun: 
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	9;Error index in IDT
	jmp interuptServiceRoutine;
invalid_TSS:
	 
	;don't pad as cpu pushes error code
	push	10;Error index in IDT
	jmp interuptServiceRoutine;
segment_not_present: 
	 
	;don't pad as cpu pushes error code
	push	11;Error index in IDT
	jmp interuptServiceRoutine;
stack_segment:
	 
	;don't pad as cpu pushes error code
	push	12;Error index in IDT
	jmp interuptServiceRoutine;
general_protection:
	 
	;don't pad as cpu pushes error code
	push	13;Error index in IDT
	jmp interuptServiceRoutine;
page_fault:
	 
	;don't pad as cpu pushes error code
	push	14;Error index in IDT
	jmp interuptServiceRoutine;
unknown_interupt:
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	16;Error index in IDT
	jmp interuptServiceRoutine;
coprocessor_error:
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	16;Error index in IDT
	jmp interuptServiceRoutine;
alignment_check:
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	17;Error index in IDT
	jmp interuptServiceRoutine;
machine_check:
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	18;Error index in IDT
	jmp interuptServiceRoutine;
simd_coprocessor_error:
	 
	push	0;Pad to make stack aligned due to no error on stack
	push	19;Error index in IDTreserved:;the same code will handle all of the reserved entrys even though they have different indexs
	jmp interuptServiceRoutine;
reserved:
	 
	push	0;pad to make stack aligned due to no error on stack
	push	20;Error index in IDT
	jmp interuptServiceRoutine;
irq0:
	push	0;pad to make stack aligned due to no error on stack
	push	32;index in IDT
	jmp interuptServiceRoutine;
irq1:
	push	0;pad to make stack aligned due to no error on stack
	push	33;index in IDT
	jmp interuptServiceRoutine;
irq2:
	push	0;pad to make stack aligned due to no error on stack
	push	34;index in IDT
	jmp interuptServiceRoutine;
irq3:
	push	0;pad to make stack aligned due to no error on stack
	push	35;index in IDT
	jmp interuptServiceRoutine;
irq4:
	push	0;pad to make stack aligned due to no error on stack
	push	36;index in IDT
	jmp interuptServiceRoutine;
irq5:
	push	0;pad to make stack aligned due to no error on stack
	push	37;index in IDT
	jmp interuptServiceRoutine;
irq6:
	push	0;pad to make stack aligned due to no error on stack
	push	38;index in IDT
	jmp interuptServiceRoutine;
irq7:
	push	0;pad to make stack aligned due to no error on stack
	push	39;index in IDT
	jmp interuptServiceRoutine;
irq8:
	push	0;pad to make stack aligned due to no error on stack
	push	40;index in IDT
	jmp interuptServiceRoutine;
irq9:
	push	0;pad to make stack aligned due to no error on stack
	push	41;index in IDT
	jmp interuptServiceRoutine;
irq10:
	push	0;pad to make stack aligned due to no error on stack
	push	42;index in IDT
	jmp interuptServiceRoutine;
irq11:
	push	0;pad to make stack aligned due to no error on stack
	push	43;index in IDT
	jmp interuptServiceRoutine;
irq12:
	push	0;pad to make stack aligned due to no error on stack
	push	44;index in IDT
	jmp interuptServiceRoutine;
irq13:
	push	0;pad to make stack aligned due to no error on stack
	push	45;index in IDT
	jmp interuptServiceRoutine;
irq14:
	push	0;pad to make stack aligned due to no error on stack
	push	46;index in IDT
	jmp interuptServiceRoutine;
irq15:
	push	0;pad to make stack aligned due to no error on stack
	push	47;index in IDT
	jmp interuptServiceRoutine;

test_sys_call:
	 
	push	0;pad to make stack aligned due to no error on stack
	push	128;Error index in IDT
	jmp interuptServiceRoutine;




interuptServiceRoutine:
	;save all normal registers
	pusha 
	;save the data and extra segment registers from error point
	push ds
    push es
    push fs
    push gs

	;Load the Kernel Data Segment descriptor!
    mov ax, 0x10   
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
	;setup the required elements for calling a c function
    mov eax, esp   
    push eax
    mov eax, exception_handler
    call eax       ; A special call, preserves the 'eip' register

	;clean up the stack
    pop eax
    pop gs
    pop fs
    pop es
    pop ds
    popa

    add esp, 8     ; Cleans up the pushed error code and pushed ISR number
    iret           ; pops 5 things at once: CS, EIP, EFLAGS, SS, and ESP!
