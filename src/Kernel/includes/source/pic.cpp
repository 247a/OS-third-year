
#include "../headers/pic.h"
namespace pic{
	//Byte operatyions only
	uint16_t MasterDataPort=0x21;
	uint16_t MasterCommandPort=0x20;
	uint16_t SlaveDataPort=0xA1;
	uint16_t SlaveCommandPort=0xA0;

	//Reasons for values in report....or at the very lest in my random notes section as very complex for somthing which most people just take to be these numbers.
	char remapCommand = 0x11;
	char masterIRQlink = 0x04;
	char slaveIRQlink = 0x02;
	char enable80x86 = 0x01;

	char IRQComplete = 0x20;

	char oldModeMasterOffset;
	char oldModeSlaveOffset;

	/**
	 * Responcible for changeing the offset of the IRQs
	 * @pram offset offset into the IDT for the pics interupts
	 */
	void remap(char offset){
		//put the slaves offset 8 entries farther on to avoid conflicts as the master has 8 possible IRQs
		remap(offset,offset+8);	
	}
	/**
	 * Responcible for changeing the offset of the IRQs
	 * @pram MasterOffset the offset into the IDT for the master pic's interupts
	 * @pram SlaveOffset the offset into the IDT for the slave pic's interupts
	 */
	void remap(char MasterOffset,char SlaveOffset){
		//------------
		//ICW1
		//------------
		ports::outb(MasterCommandPort,remapCommand);//Tell the Master PIC we wish to remap it's IRQs
		ports::portSleep();//give the pic chance to recive the message
		ports::outb(SlaveCommandPort,remapCommand);//Tell the Slave PIC we wish to remap it's IRQs
		ports::portSleep();//give the pic chance to recive the message

		
		//------------
		//ICW2
		//------------
		//Tell the master data port to add the offset to its IRQs
		//So IRQ0 with offset 0x20 would map to 0x20 on the IDT
		ports::outb(MasterDataPort,MasterOffset);
		ports::portSleep();//give the pic chance to recive the message

		//The master pic has 8 possible IRQs so add the Slaves IRQs after the masters
		ports::outb(SlaveDataPort,SlaveOffset);
		ports::portSleep();//give the pic chance to recive the message

		//as a note the Slave Has 8 possible IRQs also
		//this makes the total possible IRQs to be 16

		
		//------------
		//ICW3
		//------------
		//Tell the master and slave how they are connected to each other.
		ports::outb(MasterDataPort,masterIRQlink);
		ports::portSleep();//give the pic chance to recive the message

		ports::outb(SlaveDataPort,slaveIRQlink);
		ports::portSleep();//give the pic chance to recive the message

		//------------
		//ICW4
		//------------
		//Tell the master and slave teh addictional config of enable 80x86 mode
		ports::outb(MasterDataPort,enable80x86);
		ports::portSleep();//give the pic chance to recive the message

		
		ports::outb(SlaveDataPort,enable80x86);
		ports::portSleep();//give the pic chance to recive the message

		//------------
		//OCW4
		//------------
		//Tell the PICs which IRQs to forward to the CPU
 		ports::outb(MasterDataPort,0xfD);
		ports::portSleep();//give the pic chance to recive the message

		
		ports::outb(SlaveDataPort,0xff);
		ports::portSleep();//give the pic chance to recive the message
	}

	/**
	 * Responcible for letting the PICs know the interupt handler
	 * 		has finished with that IRQ and to send the next one
	 * @pram IRQnum id of the interupt handeled
	 */
	void interurptComplete(char IRQnum){
		//If the IRQ number is grater then 7 it means it came from the slave pic
		//Hence we would need to let the slave know as well as the master that we are finished
		if(IRQnum>IRQ7){
			ports::outb(SlaveCommandPort,IRQComplete);
		}
		ports::outb(MasterCommandPort,IRQComplete);	
	}
}
