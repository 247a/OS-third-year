#include "../headers/screen.h"
#include "../headers/memory.h"
namespace screen {
	/*
	TODO: Convert into a singlton object
	*/	

	/**
	 * Cursor Y pos
	 */
	uint8_t y = 0;
	/**
	 * Cursor X pos
	 */
	uint8_t x = 0;


	/**
	 * Maximum width of screen's printing area
	 */
	const uint8_t max_y = 25;
	/**
	 * Maximum hight of screen's printing area
	 */
	const uint8_t max_x = 80;
	/**
	 * Sets the current posistion on the screen to continue printing from
	 * @param newX the column to print from
	 * @param newY the row to print from
	 */
	void setXY(uint8_t newX, uint8_t newY) {
		if (newX<max_x && newY<max_y) {
			x = newX;
			y = newY;
		}
	}

	/**
	*Converts the 4bits passed to hex and prints the hex to screen
	*As a safty mecanisem it masks to 0x0F so that if the top four bits are set no unexpected behavior will occure.
	*
	*@deprecated: it is unlikely this is the function that calling code should call
	*		it is likely callers are looking for printHexChar(char,consoleColor,consoleColor)
	*		unless they are from this namespace
	*@pram digit: the bottom four bits of this value will be converted to hex and printed to the screen
	*@pram screen_index: the location on the screen to print to
	*@pram attributeByte: the color of the caractor to print to the screen
	*/
	void printHexDigit(char digit, uint16_t screen_index, uint8_t attributeByte) {
        	char* videoRamPointer = (char*)0xb8000;
		//Mask to only the lower 4 bits(aka last char)
		digit &= 0x0F;
		//if it is in the letter range of hex then fix the offset for letters in the ascii table
		if (digit >= 0x0A) {
			digit += 7;
		}
		//fix the offset from current value to the location in the ascii table
		digit += 48;
		videoRamPointer[screen_index] = digit;
		//attribute-byte reset
		videoRamPointer[screen_index++] = attributeByte;
	}
	/**
	 * Converts the colour enum to the value which the GPU expects
	 * @param forGound The enum value for the foreground
	 * @param backGround the enum value for the background
	 * @return the value which is the combination of the two colour enums in the form the GPU expects
	 */
	uint8_t screenMakeAttributeByte(enum consoleColor forGound, enum consoleColor backGround) {
		return forGound | backGround << 4;
	}
	/**
	 * Pints a string to the console in a requested color followed by a new line
	 * @param message ponter to the message to write
	 * @param size the number of charators to print
	 * @param forGound color of the foreground
	 * @param backGround color of the background
	 */
	void printl(char* message, int size, enum consoleColor forGound, enum consoleColor backGround) {
		print(message, size, forGound, backGround);//use prewritten print function
        uint8_t attributeByte = screenMakeAttributeByte(forGound, backGround);
        //add newline
        newLine(attributeByte);
	}
/**
	 * Pints a string to the console in a requested color
	 * @param message ponter to the message to write
	 * @param size the number of charators to print
	 * @param forGound color of the foreground
	 * @param backGround color of the background
	 */
	void print(char* message, int size, enum consoleColor forGound, enum consoleColor backGround) {
        char* videoRamPointer = (char*)0xb8000;
		uint16_t screen_index = ((y * max_x) + x) * 2; //Jump to current position
		uint8_t attributeByte = screenMakeAttributeByte(forGound, backGround);
		for (int message_index = 0; message_index < size; message_index++ , screen_index += 2) {
			// set character
		    char letter = message[message_index];
		    if(letter != '\n') {
		        videoRamPointer[screen_index] = letter;
		        //attribute-byte reset
		        videoRamPointer[screen_index + 1] = attributeByte;
		        if (x == max_x-1) {//if at end of line
		            if (y == max_y-1) {//if last line
		                //we know we are on last line so just remove
		                //  the number of chars on the line as the Y is un changed but X has
		                screen_index-=max_x*2;
		            }
		            newLine(attributeByte);//add newline
		        } else {
		            x++;
		        }
		    }else{
		        newLine(attributeByte);
		        screen_index = ((y * max_x) + x) * 2;//Recalculate index
		        screen_index -= 2;//fix for the increment via for loop
		    }
		}
	}
	/**
	 * Writes a new line to the screen in the requested color
	 * The color is i the form the GPU expects
	 * @param attributeByte the color of the newline in the form the GPU expects
	 */
	void newLine(uint8_t attributeByte) {
        x = 0;
        if (y == max_y-1) {
            char *videoRamPointer = (char *) 0xb8000;
            //Clear new bottom line
            uint16_t i =( (max_x * 2) * (max_y - 1) ) + 1;//Skip over till last line
            //Shift up one line
            memcp(videoRamPointer + (max_x * 2), videoRamPointer, i);

            while (i < max_x * max_y * 2) {
                // blank character
                videoRamPointer[i] = 'x';
                i++;
                //attribute-byte reset
                videoRamPointer[i] = attributeByte;
                i++;
            }
        }else{
            y++;
        }
	}
	/**
	 * Clears all text from the screen, and sets the color to white text on black background
	 */
	void clearScreen() {
        char* videoRamPointer = (char*)0xb8000;
		uint16_t i = 0;
		uint8_t attributeByte = screenMakeAttributeByte(white, black);
		while (i < max_x * max_y * 2) {
			// blank character
			videoRamPointer[i] = ' ';
			i++;
			//attribute-byte reset
			videoRamPointer[i] = attributeByte;
			i++;
		}
		x=0;//reset indexs
		y=0;//reset indexe
	}
	
	//Converted asm print reg to c
	/**
	 * Prints a raw memory rejion to the screen
	 * @param ptr pointer to the memory region to print
	 * @param size amount of the rejoin to print
	 */
	void printMem(char* ptr, int size) {
		int ii = 0;
		uint16_t screen_index = y * x * 2; //Jump to current position
		uint8_t attributeByte = screenMakeAttributeByte(white, black);
		//only print till end of screen as if we scroll then the user will not know as no indication of if scrolled and if so by how much
		while ((screen_index < ((max_x * max_y) - 2) * 2) && ii < size) {
			char mem = (ptr[ii] & 0xF0) >> 4;
			printHexDigit(mem, screen_index, attributeByte);

			x++;
			if (x == max_x) {//if at end of line
				if (y != max_y-1) {//if at end of line on last line just exit loop
					screen_index += 2;//just move to next char
				}else{
					break;
				}
				newLine(attributeByte);//add newline
			} else {
				screen_index += 2;//just move to next char
			}
				

			mem = (ptr[ii] & 0x0F);
			printHexDigit(mem, screen_index, attributeByte);

			x++;
			if (x == max_x) {//if at end of line
				if (y != max_y-1) {//if at end of line on last line just exit loop
					screen_index += 2;//just move to next char
				}else{
					break;
				}
				newLine(attributeByte);//add newline
			} else {
				screen_index += 2;//just move to next char
			}

            		ii++;
		}
	}
	/**
	*Prints the hex of a given char to the screen in a given color
	*@pram digit: the charactor to convert to hex and show on screen
	*@pram forGound: the color of the charator
	*@pram backGround: the color of the background for the charator
	*/
	void printHexChar(char digit, enum consoleColor forGound, enum consoleColor backGround) {
		uint8_t attributeByte = screenMakeAttributeByte(white, black);

		//print first four bits in hex
		printHexDigit(digit >> 4, ((y * max_x) + x) * 2, attributeByte);
		x++;
		if (x == max_x) {//if at end of line
			newLine(attributeByte);//add newline
		}
		//print second four bits in hex	
		printHexDigit(digit, ((y * max_x) + x) * 2, attributeByte);
		x++;
		if (x == max_x) {//if at end of line
			newLine(attributeByte);//add newline
		}
	}

	
}
