#include "../headers/mbr.h"
namespace mbr{
    /**
     * The memory location of which the MBR is stored.
     */
	mbr_entry* mbr=(mbr_entry*)0x7DBE;
	/**
	 * Simple function to use the MBR to return a pointer to the start of the kernel.
	 * @return a pointer to the start of the kernel.
	 */
	void* getKernelStart(){
		long ptr = mbr[0].LBA_start*512;
		ptr +=0x7c00;
		return (void*)ptr;
	}
	/**
	 * Uses the MBR to reveal how large the  MBR is.
	 * @return the size of the kernel.
	 */
	unsigned long getKernelSize(){
		return mbr[0].size;
	}
	/**
	 * Simple function to use the MBR to return a pointer to the start of the Elf file.
	 * @return a pointer to the start of the Elf.
	 */
	void* getElfStart(){
		long ptr = mbr[1].LBA_start*512;
		ptr +=0x7c00;
		return (void*)ptr;
	}

	/**
	 * Uses the MBR to reveal how large the test Elf file is.
	 * @return size of the test Elf file.
	 */
	unsigned long getElfSize(){
		return mbr[1].size;
	}
}
