#include "../headers/ports.h"
/**
 *Simple wrapper for Intel x86 port commands
 */
namespace ports{
	/**
	 * Prints a byte to a CPU port
	 * @param port port id
	 * @param data data to send to the port
	 */
	void outb(uint16_t port,uint8_t data){
		asm("outb %0, %1"::"a" (data),"Nd" (port));
	}
	/**
	 * prints a word to a CPU port
	 * @param port port id
	 * @param data data to send to the port
	 */
	void outw(uint16_t port,uint16_t data){
		asm("outw %0, %1"::"a" (data),"Nd" (port));
	}
	/**
	 * sends a long to a CPU port
	 * @param port port id
	 * @param data data to send to the port
	 */
	void outl(uint16_t port,uint32_t data){
		asm("outl %0, %1"::"a" (data),"Nd" (port));
	}

/**
	 * reads a byte from a CPU port
	 * @param port port id
	 * @return byte read from port
	 */
	uint8_t inb(uint16_t port){
		uint8_t data;
		asm("inb %1, %0":"=a" (data):"Nd" (port));
		return data;
	}
	/**
	 * reads a word from a CPU port
	 * @param port port id
	 * @return word read from port
	 */
	uint16_t inw(uint16_t port){
		uint16_t data;
		asm("inw %1, %0":"=a" (data):"Nd" (port));
		return data;
	}
	/**
	 * reads a long from a CPU port
	 * @param port port id
	 * @return long read from port
	 */
	uint32_t inl(uint16_t port){
		uint32_t data;
		asm("inl %1, %0":"=a" (data):"Nd" (port));
		return data;
	}

	/**
	 * Used to let the port have time to respond if needed
	 */
	void portSleep(){
		//Just use adlest two clock cycles jmping through this assmbly (which is similer to the final way that linux performs it's io_delay)
		 asm volatile ( "jmp 1f\n\t"
                   "1:jmp 2f\n\t"
                   "2:" );
		
		//another way to do this (but i don't fully trust yet as i don't know what ports are used for what other than the pic yet)
		//is to write to to an unused port, this has the advantage of being CPU speed independant.
		//This is the way that the linux kernel does this but even then they add an edge case as some computers lock up when port 0x80 is used (https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.9.2.tar.xz  :: /linux-4.9.2/arch/x86/kernel/io_delay.c)
		//asm volatile ( "outb %%al, $0x80" : : "a"(0) );
	}
}
