#include "../headers/ringSwitch.h"
/**
 * Call the assembly code to switch the cpu to userland
 */
extern "C" void _ring3();
namespace ringSwitch{
	/**
	 * Switchs the CPU to userland
	 * then calls the passed function pointer
	 * on the return of the user function halt the cpu
	 * @param userfunction user function to call after cpu switches to ring 3
	 */
	void enter_usermode (void (*userfunction)()) {
		// important note asm format
		//command source dest

		//it is nor like nasm which is dest first!
		//Also %% means register and $ means const
		// and % mean variable
		_ring3();
		userfunction();

		asm("int $128");
	}

}
