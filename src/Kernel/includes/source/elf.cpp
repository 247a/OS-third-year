#include "../headers/elf.h"
#include "../headers/screen.h"
#include "../headers/memory.h"
#define NULL 0
/**
 * This namespace is responcible for loading elf files
 */
namespace elf{
    /**
     * Prints all the relevent infomation on a passed elf in the 32 bit format
     * @param header A pointer to a elf header
     */
	void print32ElfInfo(Elf32Header* header){
		if(header->header.endian == ELF_ENDIAN_LITTLE){
			char Msg[] = "little endian";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(header->header.endian == ELF_ENDIAN_BIG){
			char Msg[] = "big endian";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else{
			char Msg[] = "Invalid setting for endian";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
			return;//Excape as invalid elf
		}
		
		if(header->header.elfVersion == ELF_ELF_VERSION_CURRENTLY_SUPPORTED){
			char Msg[] = "Elf version: 1";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else{
			char Msg[] = "Unsupported Version of elf";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
			return;
		}
		
		if(header->exeType == ELF_EXECUTION_TYPE_RELOCATABLE){
			char Msg[] = "relocatable";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(header->exeType == ELF_EXECUTION_TYPE_EXECUTABLE){
			char Msg[] = "executable";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(header->exeType == ELF_EXECUTION_TYPE_SHARED){
			char Msg[] = "shared";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
			//Not dealing will dlls yet
		}else if(header->exeType == ELF_EXECUTION_TYPE_CORE){
			char Msg[] = "core";//The spec states this exists but that it's contens has yet to be defined
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else{
			char Msg[] = "Invalid setting for exeType";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
			return;
		}






		if(header->instructionSet == ELF_INSTRUCTION_x86){
			char Msg[] = "target system x86";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(header->instructionSet == ELF_INSTRUCTION_IA_64){
			char Msg[] = "target system IA-64(unsupported)";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(header->instructionSet == ELF_INSTRUCTION_X86_64){
			char Msg[] = "target system x86-64(unsupported)";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
			//Not dealing will dlls yet
		}else if(header->instructionSet == ELF_INSTRUCTION_AArch64){
			char Msg[] = "Target system AArch64 aka 64bit arm (unsupported)";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else{
			char Msg[] = "Target System Unknown";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}

		
	}

    /**
     * Prints all the relevent infomation on the passed elf
     * @param ptr A pointer to a elf header
     */
	void printElfInfo(void* ptr){
			
		unsigned char headerType = elf::headerType(ptr);
		if(headerType==ELF_32BIT_INVALID){
			char ElfMsg[] = "elf invalid";
			screen::printl(ElfMsg,sizeof(ElfMsg),screen::white,screen::black);
			return;
		}else if(headerType==ELF_32BIT_32BIT){
			char ElfMsg[] = "elf valid 32bit";
			screen::printl(ElfMsg,sizeof(ElfMsg),screen::white,screen::black);
		}else if(headerType==ELF_32BIT_64BIT){
			char ElfMsg[] = "elf valid 64bit";
			screen::printl(ElfMsg,sizeof(ElfMsg),screen::white,screen::black);
			return;
		}

		print32ElfInfo((Elf32Header*)ptr);
		
	}


    /**
     * Allocates a block of memory to store the program sections
     * Copies the relivent data to the allocated memory blocks
     * returns a pointer to the start of the allocated memory
     * @param elf32Header pointer to the start of the source elf file
     * @return pointer to loaded memory address
     */
    void * load32ElfProgramSections(Elf32Header * elf32Header ){
        //Load program header
        Elf32ProgramHeaderTable *programHeaderTable = (Elf32ProgramHeaderTable *) (elf32Header +
                elf32Header->programHeaderPosition);
        uint32_t size = 0;
        uint32_t programHeaderIndex;
        //Loop through each program header to get an aproximation of how much memory is reqired
        for (programHeaderIndex = 0; programHeaderIndex < elf32Header->numEntryProgramHeader; programHeaderIndex++) {
            //If it is a program load entry
            if (programHeaderTable[programHeaderIndex].headerType == 0) {
                //validate that the size on disk is smaller or equal to size in mem
                if (programHeaderTable[programHeaderIndex].sizeOfSectionInFile >
                    programHeaderTable[programHeaderIndex].sizeOfSectionInMemory) {
                    //if larger fail fast
                    return NULL;//fail as load section is larger on disk than in mem
                }
                size += programHeaderTable[programHeaderIndex].sizeOfSectionInMemory;
            }
        }
        //Now the amount of memory is known allocate
        void *elfDestMem = malloc(size);
        //zero allocated size
        memset(elfDestMem, 0x00, size);

        //Begin loading the sections
        for (programHeaderIndex = 0; programHeaderIndex < elf32Header->numEntryProgramHeader; programHeaderIndex++) {
            //If it is a program load entry
            if (programHeaderTable[programHeaderIndex].headerType == 0) {
                char * sourceStart = (char*)elf32Header+programHeaderTable[programHeaderIndex].fileOffset;
                char * destStart = (char*)elfDestMem +programHeaderTable[programHeaderIndex].virtualAddr;
                memmove(sourceStart,destStart,programHeaderTable[programHeaderIndex].sizeOfSectionInFile);

                //When/If LDT gets implemented add read write execute perms on memory with given flags
            }
        }
        return elfDestMem;
    }



/*
Elf Spec page 1-4
elf32header->e_entry This member gives the virtual address to which the system first transfers control, thus
starting the process. If the file has no associated entry point, this member holds zero.
*/
	/**
	 * Responcible for correcting the memory addresses of the loaded
	 * elf file so that jmp statements go to the correct location
	 * @param elf32Header Pointer to the elf file's header
	 * @param sectionHeaderTable Location of the section header table into the elf file
	 * @param symbols Location of the symbols table into the elf file
	 * @param strings Location of the strings table into the elf file
	 * @param elfDestMem pointer to the location in which the elf was loaded
	 */
    void correctMemoryAddressOffsets(Elf32Header* elf32Header, Elf32SectionHeaderTable* sectionHeaderTable,Elf32SymbolHeaderTable* symbols,char* strings,void*elfDestMem){
        for (int i = 0; i < elf32Header->numEntrySectionHeader; ++i) {
            if (sectionHeaderTable[i].type == ELF_SECTION_RELOCATIABLE) {
               /*  Elf32_Rel* relocatiableSection = (Elf32_Rel*)(elf32Header + sectionHeaderTable[i].fileOffset);
                 for(int j = 0; j < sectionHeaderTable[i].size / sizeof(Elf32_Rel); j ++) {
                        const char* sym = strings + symbols[ELF32_R_SYM(relocatiableSection[j].r_info)].nameOffset;
                        switch(ELF32_R_TYPE(rel[j].r_info)) {
                            case R_386_JMP_SLOT:
                            case R_386_GLOB_DAT:
                                *(Elf32_Word*)(elfDestMem + relocatiableSection[j].r_offset) = (Elf32_Word)dlsym(NULL, sym);
                                break;
                        }
                 }*/

            }
        }
    }




    /**
     * Sets up an Elf file in memory and returns a pointer to the entry point
     * @param elf32Header a pointer to the start of the elf file in memory
     * @return a pointer to the entry point of the elf file ready for calling
     * @note
     * Rough description of algorithem
     * -------------------------------
     * Steps to load elf file for 32 bit mode
     * Validate elf header
     * allocate enough ram for entire elf file (Zeroed)
     * Load the program header which is elf start + the proggram header offset
     * This header is the first in an array so loop though each using the number of program headers gained form the elf header
     *  if the header type is LOAD then
     *      make sure reported sizes make seance
     *      move section from offset of file to offset from allocated mem
     *      add Read write execute flags to memory (flat memory design so no worrys)
     *
     *Next go through each Section in each section header table checking for the main function in sections of type dynsym aka dynamic symbol
     *  to check if the symbol is found the offset of the name is added to the string table address(the section of which will be linked to by this section).
     *  if the symbol is found
     *      the function pointer to call is the start of the allocated memory + the offset from the symbol header
     *
     *Next go through each relocatable section
     *  for each entry(th count of which is the size of the section div the size of a relocatable)
     *     !!!!!!!!!NOT CALCULATED!!!!!!
     *         relocate function
     *
     *Finally Call the entry point passing three prams int argcount, char ** argvals, and char ** environment prams
     *
     */
    void* load32Elf(Elf32Header* elf32Header){
        void * elfEntryPoint = NULL;
		if(elf32Header->exeType == ELF_EXECUTION_TYPE_EXECUTABLE) {
            //Load the elf sections into memory
            void *elfDestMem = load32ElfProgramSections(elf32Header);
            //If the elf loaded sucessfully
            if(elfDestMem!=NULL){
                Elf32SectionHeaderTable* sectionHeaderTable =(Elf32SectionHeaderTable*)elf32Header+elf32Header->sectionHeaderPosition;
                Elf32SymbolHeaderTable* symbols;
                char* strings = NULL;
                unsigned int i;
                //Loop through each section to find the section for the entry point,
                //  the strings and the symbols
                for(i=0; i < elf32Header->numEntrySectionHeader; ++i) {
                    if (sectionHeaderTable[i].type == ELF_SECTION_DYNAMIC_SYMBOLS) {
                        symbols = (Elf32SymbolHeaderTable*)(elf32Header + sectionHeaderTable[i].fileOffset);
                        strings = (char*)elf32Header + sectionHeaderTable[sectionHeaderTable[i].linkedSection].fileOffset;
                        for(int i = 0; i < sectionHeaderTable[i].size / sizeof(Elf32SymbolHeaderTable); i ++) {
                            if (strcmp("main", strings + symbols[i].nameOffset) == 0) {
                                elfEntryPoint = elfDestMem + symbols[i].offsetLocation;
                                break;
                            }
                        }
                        break;
                    }
                }

                //Validaite we got the entry point
                //if not free the memory used to store the elf and exit
                if(elfEntryPoint!=NULL){
                    correctMemoryAddressOffsets(elf32Header,sectionHeaderTable,symbols,strings,elfDestMem);
                }else {
                    //if we could not find the entry point then freen the memory so that there is no memory leak
                    //then just fall though the function with a null return
                   free(elfDestMem);
                }
            }
        }
        return elfEntryPoint;
    }

    /**
     * Checks if my elf loader can handle the passed elf file
     * @param header header of the elf to load
     * @return If the elf loader can handel the elf
     */
    bool checkValidaty32bit(Elf32Header* header){
		//Machine
		if(header->header.endian != ELF_ENDIAN_LITTLE ||
			header->header.elfVersion != ELF_ELF_VERSION_CURRENTLY_SUPPORTED || 
			header->instructionSet != ELF_INSTRUCTION_x86){
			return false;
		}
		//Loader
		if(header->exeType != ELF_EXECUTION_TYPE_RELOCATABLE && header->exeType != ELF_EXECUTION_TYPE_EXECUTABLE){
			return false;
		}
		return true;
    }

    /**
     * Loads elf sections into memory and executes them
     * @param header the location in which to find the elf file in memory.
     * @param argc the number of arguments
     * @param argv a pointer to the arguments to pass to the elf file
     * @param envp a pointer to the environment pramaters
     * @return The result of the elf file, or -247 if the elf could not be loaded
     */
    int loadElf(void* header, int argc,char** argv, char** envp){
        int (*elfEntry)(int, char **, char**);
        elfEntry = (int (*)(int, char **, char**))loadElf(header);
        if(elfEntry!=NULL){
            return elfEntry(argc,argv,envp);
        }else{
            return -247;//custom error code.
        }

    }

    /**
     * Returns a pointer to the entry point or null
     * @param ptr pointer to the start of the elf file in memory
     * @return a pointer to the entry point of the loaded elf file ready for execution
     */
    void* loadElf(void* ptr){
        void * returnptr = NULL;
		if(headerType(ptr)==ELF_32BIT_32BIT){
			Elf32Header* header = (Elf32Header*) ptr;
			if(checkValidaty32bit(header)){
                returnptr = load32Elf(header);
			}else{
				char Msg[] = "Elf not valid for this machine";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
			}
		}else{
			//64bit or invalid
			char Msg[] = "Elf not valid for this machine";
	       		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}
        return returnptr;
    }

    /**
     * Checks if header is valid and if so returns the type which the header is
     * @param ptr the pointer to the header to validate
     * @return A value which refers to if the header is valid, x86 or x64
     */
	unsigned char headerType(void* ptr){
		ElfHeader* header = (ElfHeader*) ptr;
		if(header->magic[0] == 0x7F &&//check first 4 bytes are 0x7f followed by ELF
			header->magic[1] == 0x45 &&
			header->magic[2] == 0x4C &&
			header->magic[3] == 0x46 &&
			header->is32bit<3){
			//header->is32bit<3 is due to if it is less than 3 when we return we will either return 0 aka invalid 1 aka 32bit or 2 aka 64bit
			
			return header->is32bit;
		}else{
			return ELF_32BIT_INVALID;
		}
	}
	
}
