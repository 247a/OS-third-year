#include "../headers/exception_consts.h"
#include "../headers/exception_handler.h"
#include "../headers/pic.h"
#include "../headers/screen.h"
#include "../headers/keyboard.h"
namespace exceptionHandler{
	/**
	 * Interupt handeler for CPU error codes.
	 * Prints relevent error then stops execution
	 * @param r the stack pointer to the regs struct.
	 */
	void handleError(struct regs *r){
		if(r->int_no==DIVIDE_ERROR){
			char Msg[] = "ERROR:DIVIDE_ERROR";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==DEBUG){
			char Msg[] = "ERROR:DEBUG";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==NON_MASKABLE_INTERRUPT_EXCEPTION){
			char Msg[] = "ERROR:NON_MASKABLE_INTERRUPT_EXCEPTION";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==BREAKPOINT){
			char Msg[] = "ERROR:BREAKPOINT";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==OVERFLOW){
			char Msg[] = "ERROR:OVERFLOW";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==OUT_OF_BOUNDS){
			char Msg[] = "ERROR:OUT_OF_BOUNDS";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==INVALID_OP){
			char Msg[] = "ERROR:INVALID_OP";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==DEVICE_NOT_AVAILABLE){
			char Msg[] = "ERROR:DEVICE_NOT_AVAILABLE";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==DOUBLE_FAULT){
			char Msg[] = "ERROR:DOUBLE_FAULT";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==COPROCESSOR_SEGMENT_OVERRUN){
			char Msg[] = "ERROR:COPROCESSOR_SEGMENT_OVERRUN";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==INVALID_TSS){
			char Msg[] = "ERROR:INVALID_TSS";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==SEGMENT_NOT_PRESENT){
			char Msg[] = "ERROR:SEGMENT_NOT_PRESENT";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==STACK_FAULT){
			char Msg[] = "ERROR:STACK_FAULT";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==GENERAL_PROTECTION){
			char Msg[] = "ERROR:GENERAL_PROTECTION - are you calling something bad in ring 3?";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==PAGE_FAULT){
			char Msg[] = "ERROR:PAGE_FAULT";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==UNKNOWN_INTERUPT){
			char Msg[] = "ERROR:UNKNOWN_INTERUPT";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==COPROCESSOR_ERROR){
			char Msg[] = "ERROR:COPROCESSOR_ERROR";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==ALIGNMENT_CHECK){
			char Msg[] = "ERROR:ALIGNMENT_CHECK";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==MACHINE_CHECK){
			char Msg[] = "ERROR:MACHINE_CHECK";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else if(r->int_no==SIMD_COPROCESSOR_ERROR){
			char Msg[] = "ERROR:SIMD_COPROCESSOR_ERROR";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}else{
			char Msg[] = "Fault handeled Genericly - Likely reserved error or bug";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}
	
		for (;;);
	}
/**
 * Handler for interupts origanateing from the PIC
 * @param r the stack pointer to the regs struct.
 */
	void handelIRQs(struct regs *r){
		if(r->int_no==IRQ0){
			char Msg[] = "IRQ0 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ1){//Handle a keyevent
			keyboard::handleIRQ();			
		}else if(r->int_no==IRQ2){
			char Msg[] = "IRQ2 handeled Genericly - This should not be possible to fire...";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ3){
			char Msg[] = "IRQ3 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ4){
			char Msg[] = "IRQ4 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ5){
			char Msg[] = "IRQ5 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ6){
			char Msg[] = "IRQ6 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ7){
			char Msg[] = "IRQ7 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ8){
			char Msg[] = "IRQ8 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ9){
			char Msg[] = "IRQ9 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ10){
			char Msg[] = "IRQ10 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ11){
			char Msg[] = "IRQ11 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ12){
			char Msg[] = "IRQ12 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ13){
			char Msg[] = "IRQ13 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ14){
			char Msg[] = "IRQ14 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else if(r->int_no==IRQ15){
			char Msg[] = "IRQ15 handeled Genericly";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);			
		}else {
			char Msg[] = "IRQ with unknown id handeled Genericly - BUG";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
		}
		pic::interurptComplete(r->int_no);
	}
/**
 * Handler for system calls
 * These often originate from userland to perform privalaged operatios
 * @param r the stack pointer to the regs struct.
 */
	void handleSyscall(struct regs *r){
		if(r->int_no==128){
			char Msg[] = "exit handeled";
			screen::printl(Msg,sizeof(Msg),screen::white,screen::black);	
			for(;;) {
				//asm("hlt");
			} 			
		}
		char Msg[] = "Syscall handeled Genericly";
		screen::printl(Msg,sizeof(Msg),screen::white,screen::black);
	}

	extern "C"{
		/**
		 * Handles the interupt passed from the IDT
		 *
		 * and calles the relevent sub handler for teh clasification of the interupt
		 * @param r the stack pointer to the regs struct.
		 */
		void exception_handler(struct exceptionHandler::regs *r){

			if(r->int_no<32){//exception hence halt on exit
				handleError(r);
			}else if(r->int_no>=IRQ0 && r->int_no<=IRQ15){//its an irq
				handelIRQs(r);		
			}else{
				handleSyscall(r);
			}

		}
	}
}
