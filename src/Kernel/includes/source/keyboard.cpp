#include "../headers/keyboard.h"
#include "../headers/ports.h"
#include "../headers/screen.h"
namespace keyboard{
	#define KEYBOARD_SCANCODE_RELEASED_MASK 0x80
	//int currentMapId = 0;
	//button map wrapper by trial and error but matched to ? version on https://www.win.tue.nl/~aeb/linux/kbd/scancodes-10.html
	//hence i used the other maps they sourced to enable multiple keyboards and used my base keyboard as the wrapper keyboard type.
	/*enum buttonSetUsb{
		P = 0x19,
	};
	enum buttonSet1{

	};
	enum buttonSet2{

	};
	enum buttonSet3{
	
	};
	enum buttonSetX1{

	};
	enum buttonSetX2{

	};
	enum buttonSetX3{

	};

	enum wrapper{
		P = 0x19,
	};*/

//http://www.henkessoft.de/OS_Dev/OS_Dev1.htm#mozTocId185043
/**
 * Handles the keyboard interupt
 */
	void handleIRQ(){
		 unsigned char scan_code = ports::inb(0x60);
		 screen::printHexChar(scan_code, screen::white,screen::black);
		 char Msg[] = "Keyevent:";
		 screen::print(Msg,sizeof(Msg),screen::white,screen::black);
		 
		
		 
		 if(scan_code & KEYBOARD_SCANCODE_RELEASED_MASK){
			char keyevent[] = "released";
		 	screen::printl(keyevent,sizeof(keyevent),screen::white,screen::black);
		 }else{
			char keyevent[] = "pressed";
		 	screen::printl(keyevent,sizeof(keyevent),screen::white,screen::black);
		 }
	}

	/*void setKeyMap(int mapId){
		currentMapId = mapId;
	}
	
	int getKeyMap(){
		return currentMapId;
	}

	char* getKeyNameFromScanCode(int scanCode){
	}*/
}
