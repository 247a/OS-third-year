
#include "../headers/idt.h"
namespace idt{
	/**
	 * Used to add entries to the IDT
	 * @param num interupt id
	 * @param entry IDT entry to insert
	 * @return true if entry inserted successfully
	 */
	bool add_exception_handler(unsigned int num, idt_entry entry)
	{
		if(num>=MAX_ENTRIES)return false;
	    /* The interrupt routine's base address */
	    idt[num]=entry;
		return true;
	}
	/**
	 * Deactivates a single interupt handler
	 * @param num the interupt id of the interupt handler to disable
	 * @return true if interupt handler disabled
	 */
	bool disable_exception(unsigned int num){
		if(num>=MAX_ENTRIES)return false;
		idt[num].present=0;
		return true;
	}
	/**
	 * Re-enables a previously disabled handler
	 * @param num the interupt id of the handler to reenable
	 * @return true if re-enabled sucessfully
	 */
	bool enable_exception(unsigned int num){
		if(num>=MAX_ENTRIES)return false;
		idt[num].present=1;
		return true;
	}

	/**
	 * Updates the pointer held in an IDT entry to the pointer passed
	 * @param function new function pointer
	 * @param entry IDT entry to update
	 */
	void calculateAddress(void (*function)(),idt_entry* entry)
	{
		unsigned int base = (unsigned int)function; 
		entry->base_lo = (base & 0xFFFF);
		entry->base_hi = (base >> 16) & 0xFFFF;
	}


	/**
	 * Initalises the Interupt descripter table
	 */
	void init_exception_handler()
	{


	    // make the table empty
	    //memset(&idt, 0, sizeof(struct idt_entry) * 256);

	    //Call the function in assebly to activiate the interupt discripter table
	    loadIdt(&idtp);
	}
	/**
	 * Initalises the Interupt descripter table with the given pointer to the IDT
	 * @param idt_ref ponter object which refers to the IDT
	 */
	void loadIdt(struct idt_ptr* idt_ref){
		//depending on number of calls to and outputed asm possible perfomance enhancement here by moving the first two lines to a constructor

		//The size of the idt is the size of the structure multipiled by the number of entries
		//the -1 is to fix a off by one error
		idt_ref->size = (sizeof (idt_entry) * MAX_ENTRIES)-1;
		//set first entry
		idt_ref->start = (uint32_t)idt;
		pic::remap(bestPicOffsetBase);
		//Use gas syntax to load idt
		asm("lidt (%0)"::"r"(idt_ref));
		asm("sti");//enable hardware interupts
	}

	/**
	 * Adds the relevent entrys to the IDT
	 * Then loads the IDT
	 */
	void setUpIdt(){
		//setup basic entry and just edit what is needed to be edited per excption as things like the present flag will not change
		idt_entry entry;
		entry.always0=0;
		entry.unused=0;
		entry.present=1;
		entry.sel = 0x08;
		entry.type = IDT_TYPE_32BIT_TRAP;
		entry.ring = 0;
		//ERRORS
		calculateAddress(divide_error,&entry);
		add_exception_handler(DIVIDE_ERROR,entry);
		calculateAddress(debug,&entry);
		add_exception_handler(DEBUG,entry);

		entry.type = IDT_TYPE_32BIT_INTERRUPT;
		entry.ring = 0;
		calculateAddress(nmi,&entry);
		add_exception_handler(NON_MASKABLE_INTERRUPT_EXCEPTION,entry);
		entry.type = IDT_TYPE_32BIT_TRAP;
		entry.ring = 3;
		calculateAddress(int3,&entry);
		add_exception_handler(BREAKPOINT,entry); 
		entry.type = IDT_TYPE_32BIT_TRAP;
		entry.ring = 3;
		calculateAddress(overflow,&entry);
		add_exception_handler(OVERFLOW,entry); 
		entry.type = IDT_TYPE_32BIT_TRAP;
		entry.ring = 3;
		calculateAddress(bounds,&entry);
		add_exception_handler(OUT_OF_BOUNDS,entry); 
		entry.type = IDT_TYPE_32BIT_TRAP;
		entry.ring = 0;
		calculateAddress(invalid_op,&entry);
		add_exception_handler(INVALID_OP,entry);
		calculateAddress(device_not_available,&entry);
		add_exception_handler(DEVICE_NOT_AVAILABLE,entry);

		calculateAddress(double_fault,&entry);
		add_exception_handler(DOUBLE_FAULT,entry);

		calculateAddress(coprocessor_segment_overrun,&entry);
		add_exception_handler(COPROCESSOR_SEGMENT_OVERRUN,entry);
		calculateAddress(invalid_TSS,&entry);
		add_exception_handler(INVALID_TSS,entry);
		calculateAddress(segment_not_present,&entry);
		add_exception_handler(SEGMENT_NOT_PRESENT,entry);
		calculateAddress(stack_segment,&entry);
		add_exception_handler(STACK_FAULT,entry);
		calculateAddress(general_protection,&entry);
		add_exception_handler(GENERAL_PROTECTION,entry);
		calculateAddress(page_fault,&entry);
		add_exception_handler(PAGE_FAULT,entry);
		calculateAddress(unknown_interupt,&entry);
		add_exception_handler(UNKNOWN_INTERUPT,entry);
		calculateAddress(coprocessor_error,&entry);
		add_exception_handler(COPROCESSOR_ERROR,entry);
		calculateAddress(alignment_check,&entry);
		add_exception_handler(ALIGNMENT_CHECK,entry);
		calculateAddress(machine_check,&entry);
		add_exception_handler(MACHINE_CHECK,entry);
		calculateAddress(simd_coprocessor_error,&entry);
		add_exception_handler(SIMD_COPROCESSOR_ERROR,entry);


		//IRQs
		calculateAddress(irq0,&entry);
		add_exception_handler(IRQ0,entry);

		calculateAddress(irq1,&entry);
		add_exception_handler(IRQ1,entry);

		calculateAddress(irq2,&entry);
		add_exception_handler(IRQ2,entry);

		calculateAddress(irq3,&entry);
		add_exception_handler(IRQ3,entry);

		calculateAddress(irq4,&entry);
		add_exception_handler(IRQ4,entry);

		calculateAddress(irq5,&entry);
		add_exception_handler(IRQ5,entry);

		calculateAddress(irq6,&entry);
		add_exception_handler(IRQ6,entry);

		calculateAddress(irq7,&entry);
		add_exception_handler(IRQ7,entry);

		calculateAddress(irq8,&entry);
		add_exception_handler(IRQ8,entry);

		calculateAddress(irq9,&entry);
		add_exception_handler(IRQ9,entry);

		calculateAddress(irq10,&entry);
		add_exception_handler(IRQ10,entry);

		calculateAddress(irq11,&entry);
		add_exception_handler(IRQ11,entry);

		calculateAddress(irq12,&entry);
		add_exception_handler(IRQ12,entry);

		calculateAddress(irq13,&entry);
		add_exception_handler(IRQ13,entry);

		calculateAddress(irq14,&entry);
		add_exception_handler(IRQ14,entry);

		calculateAddress(irq15,&entry);
		add_exception_handler(IRQ15,entry);

		//SYSCALLS
		entry.type = IDT_TYPE_32BIT_INTERRUPT;
		entry.ring = 3;

		calculateAddress(test_sys_call,&entry);
		add_exception_handler(128,entry);//system_call
	
		init_exception_handler();

	}

}

