#include "../headers/gdt.h"

#include "../headers/tss.h"
namespace gdt{

    /**
     * Loades the globel descriptor table stored in gdt::Gdt into the CPU
     */
    void loadGdt(){
        //depending on number of calls to and outputed asm possible perfomance enhancement here by moving the first two lines to a constructor

        //The size of the gdt is the size of the structure multipiled by the number of entries
        //the -1 is to fix a off by one error
        gdtPointer.size = (sizeof (Gdt) * MAX_ENTRIES)-1;
        //set first entry
        gdtPointer.start = (uint32_t)gdt;
        //Use gas syntax to load gdt
        asm("lgdt (%0)"::"r"(&gdtPointer));
        //Activate new gdt
        asm("movw $0x10, %ax    \n \
            movw %ax, %ds    \n \
            movw %ax, %es    \n \
            movw %ax, %fs    \n \
            movw %ax, %gs    \n \
            ljmp $0x08, $next    \n \
            next:        \n");
    }
    /**
     * Adds an entry to the global descriptor table
     * @param sectorIndex position to insert into the table
     * @param gdt_entry entry to insert
     */
    void setSector(uint16_t sectorIndex, Gdt gdt_entry){
        if(sectorIndex<MAX_ENTRIES){
            gdt[sectorIndex] = gdt_entry;
        }
    }
    /**
     * Edits a GDT entry's start address and size to the given values
     * @param start Start address of the memory govened by the entry
     * @param size Size of the memory address governed by the entry
     * @param gdt_entry pointer to the entry to edit
     */
    void convertStartSize(uint64_t start, uint32_t size,Gdt* gdt_entry){
	gdt_entry->size1 = size & 0xffff;//get the 2 lest significant Bytes
        gdt_entry->size2 = (size>>16) & 0xf;//skip first 16bits, then grab remaining 4 bits
        gdt_entry->start1 = start & 0xffff;//get the 2 lest significant Bytes
        gdt_entry->start2 = (start>>16) & 0xff;//skip first 16bits, then grab remaining 4 bits
        gdt_entry->start3 = (start>>24) & 0xff;//skip first 16bits, then grab remaining 4 bits
	}
    /**
     * Creates an GDT entry which descibes a memory block of the size provided,with the start address provided
     * @param start start address of the memory block to be desribed
     * @param size size of the memory block to be described
     * @return the GDT entry describing the relevent memory block
     */
    Gdt convertStartSize(uint64_t start, uint32_t size){
        Gdt gdt_entry;
        convertStartSize(start,size,&gdt_entry);
        return gdt_entry;
    }
    /**
     * Creates a new gdt for the kernel and userland.
     * Also adds an entry for the Task Segment slector
     * Loads GDT and TSS.
     */
    void SetupNewGDT(){
        //Setup empty GDT entry
        Gdt gdt = convertStartSize(0x0,0x0);
        gdt.present = 0;
        gdt.ring = 0;
        gdt.executable = 0;
        gdt.descriptorType = 0;
        gdt.conforming = 0;
        gdt.ReadWrite = 0;
        gdt.accessed = 0;
        gdt.granularity = 0;
        gdt.is32bitSeg = 0;
        gdt.is64bitSeg = 0;

        setSector(0,gdt);
        //Set up Code Seg for kernel from bootloader specs
        gdt = convertStartSize(0x0,0xfffff);
        gdt.present = 1;
        gdt.ring = 0;
        gdt.descriptorType = 1;
        gdt.executable = 1;
        gdt.conforming = 0;
        gdt.ReadWrite = 1;
        gdt.accessed = 0;
        gdt.granularity = 1;
        gdt.is32bitSeg = 1;
        gdt.is64bitSeg = 0;

        setSector(1,gdt);

        //Build Data seg for kernel from Code segment and bootloader specs
        gdt.executable = 0;
        setSector(2,gdt);

        //build usermode Data seg
        gdt.ring=3;
        setSector(3,gdt);

        //build usermode code seg
        gdt.executable = 1;
        setSector(4,gdt);

        setSector(5,tss::generateTssSegment());
        loadGdt();
        tss::_installTss();
    }

}
