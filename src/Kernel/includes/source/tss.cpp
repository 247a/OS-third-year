
#include "../headers/tss.h"
#include "../headers/memory.h"
/**
 * Namespace responicble for creating the TSS
 * Which is used for getting out of userland via interupts
 */
namespace tss{
	/**
	 * the instance of the TSS
	 */
static tss_struct tss_entry;
	/**
     * Creates the entry for the GDT
     * And loads the TSS
     * @return an entry to be inserted into the GDT
     */
gdt::Gdt generateTssSegment(){
	gdt::Gdt gdt = gdt::convertStartSize((uint64_t)&tss_entry,sizeof(tss_entry));
	gdt.granularity=0;//Bytes not pages
	gdt.accessed = 1;//show it is a TSS
	gdt.ReadWrite= 0;//zero indicates not busy
	gdt.conforming=0;//always zero for TSS
	gdt.executable=1;//for a tss it is a is 32bit flag(if zero 16bit)
	gdt.descriptorType=0;//indicate a TSS
	gdt.ring=3;//Same meaning as if it was GDT as the userland can use this segment
	gdt.present=1;//show the TSS is present
	gdt.is64bitSeg = 0;//TSS is not valid in 64bit
	gdt.is32bitSeg = 0;//man says set as zero
	
	// Ensure the TSS is initially zero'd.
   	memset(&tss_entry, 0, sizeof(tss_entry));
	tss_entry.ss0  =0x10;//stack segment which is currently in the kernel data segment
//kernel data seg 2nd entry *8bits
	tss_entry.esp0 =0x90000;//kernal stack
	return gdt;
}

}
