
#include "../headers/memory.h"
const void* chainStart =   (void*)0x100000;
const void* maxMem = (void*)0x1FFFFF;
/**
 * Adds a link to the memory block chain, and updates surounding links
 * @param size number of bytes to allocate to new block
 * @param newHeaderLocation location to new link
 * @param prevHeader pointer to previous link (or chainStart if first allocated block)
 * @param nextHeader pointer to next line(or null if inserting to the end of the chain)
 * @return pointer to new data area
 */
void* addLinkToMemChain(unsigned int size,struct memoryHeader* newHeaderLocation, struct memoryHeader* prevHeader, struct memoryHeader* nextHeader=NULL){

	//make this header point ot the right last link
	newHeaderLocation->prevBlock = prevHeader;
	//if the last link was not the starting pointer then update it's refrence
	if(prevHeader!=chainStart){
		prevHeader->nextBlock = newHeaderLocation;
	}
	//set the next link in the chain
	newHeaderLocation->nextBlock = nextHeader;
	//if the next link is not null then update its refrence
	if(nextHeader!=NULL){
		nextHeader->prevBlock = newHeaderLocation;
	}
	//set the size of the data area
	newHeaderLocation->size = size;
	//return pointer to the data area of the memory block
	return newHeaderLocation+sizeof(memoryHeader);
}
/**
 * Allocates the specified number of bytes and returns a pointer to the allocated region
 * @param size number of bytes to allocate
 * @return Return pointer to allocated memory, or null if not possible
 */
void* malloc(unsigned int  size){
    void** currentPos = (void**)chainStart;   
    //if there is no chain make one 
    if(*currentPos ==NULL){
	//make the chain start pointer point to just after the chain start pointer
        *currentPos =(void*)(currentPos+1);
	
	//return pointer to start of data ( just after end of header)
        return addLinkToMemChain(size,(memoryHeader*)*currentPos,(memoryHeader*)chainStart);
    }else{//else loop through the chain

        memoryHeader *blockHeader = (memoryHeader*)*currentPos;
        //whilst there is still more chain to loop though
        while(blockHeader->nextBlock != NULL){
            //if there is space between the entries insert it
            if(blockHeader->nextBlock-(blockHeader+sizeof(memoryHeader) +1)<size){
                memoryHeader* nextBlock = blockHeader //Start at current block
                                    +sizeof(memoryHeader)//jump to data
                                    + blockHeader->size //jump past end of data
				    + 1;//jump to next free space
		//Due to finding a bit of space between entries add to the chain and updat the other links
	    	//  and return a pointer to the data area of the memory block
		return addLinkToMemChain(size,nextBlock,blockHeader,blockHeader->nextBlock);
            }else{//if there isn't space move to the next block
                blockHeader = blockHeader->nextBlock;
            }
        }
        memoryHeader* nextBlock = blockHeader //Start at current block
                                    +sizeof(memoryHeader)//jump to data
                                    + blockHeader->size //jump past end of data
				    + 1;//jump to next free space
        //only can reach here if at the end of the chain so check if out of memory
        if(nextBlock + size + sizeof(memoryHeader)<maxMem){
	    //as there is space for the new block add the new block to the block chain
	    //  and return a pointer to the data area of the memory block
            return addLinkToMemChain(size,nextBlock,blockHeader);
        }else{
            return NULL;
        }
    }
}
/**
 * Frees the memory pointed at by the given pointer
 * @param ptr pointer to the memory block to free
 */
void free(void* ptr){
    //pointer to the header is the data region header minus the size of the header
    memoryHeader *header = (memoryHeader*)ptr - sizeof(memoryHeader);
    //set the link of the previos block to the next block
    header->prevBlock->nextBlock = header->nextBlock;
    //if the next block is not null the set its link to the previos block
    if(header->nextBlock != NULL){
        header->nextBlock->prevBlock = header->prevBlock;
    }
}
/**
 * @hide
 */
void* realloc (void* ptr, unsigned int  size){
}
/**
 * @hide
 */
void* calloc(unsigned int  num, unsigned int  size){	
    unsigned int mallocSize = num*size;
	if(mallocSize>0){
	    char* pointer = (char*)malloc(mallocSize);
	    for(int i = 0; i<mallocSize; i++){
			pointer[i] = 0;
	    }
	    return pointer;
	}else{
		return NULL;
	}
}
/**
 * @hide
 */
void memcp(void* src, void* dest, int size){
    char* source = (char*)src;
    char* destination = (char*)dest;
    
    for(int i=0;i<size;i++){
        destination[i]=source[i];
    }
}
/**
 *Copies number of bytes define in size from the memory at src to the memory at dest so that dest contains exactaly what src did even if the addresses over lap.
 * @param src source address
 * @param dest destination address
 * @param size number of bytes to move
 * @note could have gone down the route of allocate additional mem copy to temp location then copy back
 * however this way is faster and can handle very large memory sizes wich the other method can't
 */
void memmove(void* src, void* dest, int size){
    if(dest>src){
		memcp(src,dest,size);
    }else{
		char* source = (char*)src;
		char* destination = (char*)dest;
		
		for(int i=size-1;i>=0;i--){
		    destination[i]=source[i];
		}
    }
}
/**
 * Sets a defined number of bytes at the given memory address to the given value
 * @param dest Address of memory to set
 * @param val value to set memory to
 * @param size number of bytes to set
 */
void memset(void* dest, char val, int size){
    char* destination = (char*)dest;
    for(int i=0;i<size;i++){
        destination[i]=val;
    }
}
/**
 * Compares two C style strings and retuns if they are the same
 * @param str1 First string to compare
 * @param str2 Second string to compare
 * @return zero if both strings are the same,
 * >0 if the first diffeing char is larger in str1,
 * <0 if the first differing char is larger in str2
 * @note based on http://www.cplusplus.com/reference/cstring/strcmp/
 */
int strcmp(const char * str1, const char * str2 ){
    int index=0;
    while(str1[index]==str2[index]&&str1[index]!=0&&str2[index]!=0){
        index++;
    }
    if(str1[index]==str2[index]){
        return 0;
    }else if(str1[index]>str2[index]){
        return 1;
    }else{
        return 0;
    }
}

