#include <stdint.h>
#ifndef GDT
#define GDT
namespace gdt{
    #define kernelCodeSegment = 0x08;
    #define kernelDataSegment = 0x10;
    #define userCodeSegment =0x20;
    #define userDataSegment =0x18;
    const uint16_t MAX_ENTRIES = 0x0f;//15 entries
    /**
     * Structure which points to the GDT
     */
    struct GdtPointer{
        //Commented with assembly counterpart
        //GDT_Descriptor:
        uint16_t size; //dw GDT_End - GDT_Start -1
        uint32_t start; //dd GDT_Start
    }__attribute__ ((packed));
    /**
     * Structure of each entry in the GDT
     */
    struct Gdt{
        unsigned int size1 : 16;//first two bytes of segment size
        unsigned int start1 : 16;//first two bytes of start address
        unsigned int start2 : 8;//third byte of start address
        //First byte of flags (bitfield)
	unsigned int accessed : 1;
        unsigned int ReadWrite : 1;
        unsigned int conforming : 1;//0 = access via current ring only 1 = current and lower
        unsigned int executable : 1;
        unsigned int descriptorType : 1;
        unsigned int ring : 2;
        unsigned int present : 1;

//Last 4 bits of segment size
        unsigned int size2 :4;
 //next 4 bits of flags
	unsigned int unused : 1;
 	unsigned int is64bitSeg : 1;
	unsigned int is32bitSeg : 1;
        unsigned int granularity : 1; // 4kb blocks(1) or byte blocks(0)
        unsigned int start3 : 8;//fourth byte of base address
    }__attribute__ ((packed));
   /**
    * poiner object to the GDT
    */
    static struct GdtPointer gdtPointer;
    /**
     * The instance of the GDT
     */
    static struct Gdt gdt[MAX_ENTRIES];
    /**
     * Creates a new gdt for the kernel and userland.
     * Also adds an entry for the Task Segment slector
     * Loads GDT and TSS.
     */
    void SetupNewGDT();
    /**
     * Loades the globel descriptor table stored in gdt::Gdt into the CPU
     */
    void loadGdt();
    /**
     * Adds an entry to the global descriptor table
     * @param sectorIndex position to insert into the table
     * @param gdt_entry entry to insert
     */
    void setSector(uint16_t sectorIndex, Gdt gdt);
    /**
      * Edits a GDT entry's start address and size to the given values
      * @param start Start address of the memory govened by the entry
      * @param size Size of the memory address governed by the entry
      * @param gdt_entry pointer to the entry to edit
      */
    void convertStartSize(uint64_t start, uint32_t size,Gdt* gdt_entry);
    /**
     * Creates an GDT entry which descibes a memory block of the size provided,with the start address provided
     * @param start start address of the memory block to be desribed
     * @param size size of the memory block to be described
     * @return the GDT entry describing the relevent memory block
     */
    Gdt convertStartSize(uint64_t start, uint32_t size);
}
#endif

