#include <stdint.h>

#ifndef PORTS
#define PORTS
/**
 *Simple wrapper for Intel x86 port commands
 */
namespace ports{
	/**
	 * Prints a byte to a CPU port
	 * @param port port id
	 * @param data data to send to the port
	 */
	void outb(uint16_t port,uint8_t data);
	/**
	 * prints a word to a CPU port
	 * @param port port id
	 * @param data data to send to the port
	 */
	void outw(uint16_t port,uint16_t data);
	/**
	 * sends a long to a CPU port
	 * @param port port id
	 * @param data data to send to the port
	 */
	void outl(uint16_t port,uint32_t data);

	/**
	 * reads a byte from a CPU port
	 * @param port port id
	 * @return byte read from port
	 */
	uint8_t inb(uint16_t port);
	/**
	 * reads a word from a CPU port
	 * @param port port id
	 * @return word read from port
	 */
	uint16_t inw(uint16_t port);
	/**
	 * reads a long from a CPU port
	 * @param port port id
	 * @return long read from port
	 */
	uint32_t inl(uint16_t port);

	/**
	 * Used to let the port have time to respond if needed
	 */
	void portSleep();
}
#endif
