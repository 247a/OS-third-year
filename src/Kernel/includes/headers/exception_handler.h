#ifndef EXCEPTION_HANDLER
#define EXCEPTION_HANDLER
#include "../headers/exception_consts.h"
#include "../headers/pic.h"
#include "../headers/screen.h"
#include "../headers/keyboard.h"
namespace exceptionHandler{
/**
	 * Structure of the stack after the assembly handeler has set it up
	 * this is passed to the C/ C++ handler
	 * from http://www.osdever.net/bkerndev/Docs/isrs.htm
	 */
	struct regs
{
    unsigned int gs, fs, es, ds;      /* pushed the segs last */
    unsigned int edi, esi, ebp, esp, ebx, edx, ecx, eax;  /* pushed by 'pusha' */
    unsigned int int_no, err_code;    /* our 'push byte #' and ecodes do this */
    unsigned int eip, cs, eflags, useresp, ss;   /* pushed by the processor automatically */ 
};
extern "C"{
		/**
		 * Handles the interupt passed from the IDT
		 * @param r the stack pointer to the regs struct.
		 */
		void exception_handler(struct exceptionHandler::regs *r);
}





}
#endif
