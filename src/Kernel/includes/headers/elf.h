#ifndef ELF
#define ELF
namespace elf{
	//typedefs taken from http://wiki.osdev.org/ELF_Tutorial
	//  but refer to http://www.skyfree.org/linux/references/ELF_Format.pdf fig 1-2
    typedef unsigned short Elf32_Half;	// Unsigned half int
    typedef unsigned int Elf32_Off;	    // Unsigned offset
    typedef unsigned int Elf32_Addr;	// Unsigned address
    typedef unsigned int Elf32_Word;	// Unsigned int
    typedef int  Elf32_Sword;	// Signed int
    
    //Strip out all the constant definitions
    #include "elf_costs.h"
    

	/**
	 * The structure of the first section of an elf file regardless of its acretechr type
	 */
    struct ElfHeader{
    	unsigned char magic[4];
	    unsigned char is32bit;

	    unsigned char endian;
	    unsigned char elfVersion;
	    unsigned char targetSystemABI;
	    unsigned char padding[8];
    }__attribute__ ((packed));

    /**
     * The first section of 32-bit elf files
     * Source:
     * linux elf.h and http://www.skyfree.org/linux/references/ELF_Format.pdf fig 1-3
     */
	struct Elf32Header{
        ElfHeader   header;
        Elf32_Half  exeType;
	    Elf32_Half  instructionSet;
	    Elf32_Word  elfVersion;
	    Elf32_Addr	programEntryAddress;
	    Elf32_Off	programHeaderPosition;
	    Elf32_Off	sectionHeaderPosition;
	    Elf32_Word	archFlags;
	    Elf32_Half	headerSize;
	    Elf32_Half	entrySizeProgramHeader;
	    Elf32_Half	numEntryProgramHeader;
	    Elf32_Half	entrySizeSectionHeader;
	    Elf32_Half	numEntrySectionHeader;
	    Elf32_Half	namesSectionIndex;
    }__attribute__ ((packed));

/**
 * Required for Execution types but not for linking (based on linux's /usr/include/elf.h)
 * Indicates executable memory regions
 */
	struct Elf32ProgramHeaderTable{
		Elf32_Word	headerType;	
  		Elf32_Off	fileOffset;		
  		Elf32_Addr	virtualAddr;		
  		Elf32_Addr	physicalAddr;		
  		Elf32_Word	sizeOfSectionInFile;	
//Can be larger than the file size or same size	as in the file
//	As the file can be compressed, which means the mem would be more but the mem would not be compressed(unless something really funky in the code occurs which i can't handle, this funky ness would be things like the elf reading direct from the hdd to load more of its code)	
  		Elf32_Word	sizeOfSectionInMemory;	
//Read, write and execute flags
  		Elf32_Word	accessFlags;		
  		Elf32_Word	alignment;		
		
	}__attribute__ ((packed));

	struct Elf32SymbolHeaderTable{
		Elf32_Word	nameOffset;		/* the name of the function*/
  		Elf32_Addr	offsetLocation;	/* ofset into file to the symbol */
  		Elf32_Word	st_size;	/* Symbol size (unused)*/
  		unsigned char	info;	/* Symbol type and binding(unused) */
  		unsigned char	other;	/* Symbol visibility (unused)*/
  		Elf32_Half   index;	/* Section index(unused) */
	}__attribute__ ((packed));


//Required for linking but not for execution
	struct Elf32SectionHeaderTable{
        Elf32_Word	nameOffset;		/* Section name (string tbl index) */
        Elf32_Word	type;		/* Section type */
        Elf32_Word	flags;		/* Section flags */
        Elf32_Addr	virtualAddr;		/* Section virtual addr at execution */
        Elf32_Off	fileOffset;		/* Section file offset */
        Elf32_Word	size;		/* Section size in bytes */
        Elf32_Word	linkedSection;		/* Link to another section */
        Elf32_Word	info;		/* Additional section information */
        Elf32_Word	addrAlign;		/* Section alignment */
        Elf32_Word	entSize;		/* Entry size if section holds table */
	}__attribute__ ((packed));

	/**
        * Prints all the relevent infomation on a passed elf in the 32 bit format
        * @param header A pointer to a elf header
        */
    void print32ElfInfo(Elf32Header* header);
	/**
      * Prints all the relevent infomation on the passed elf
      * @param ptr A pointer to a elf header
      */
    void printElfInfo(void* ptr);
	/**
     * Sets up an Elf file in memory and returns a pointer to the entry point
     * @param elf32Header a pointer to the start of the elf file in memory
     * @return a pointer to the entry point of the elf file ready for calling
     * @note
     * Rough description of algorithem
     * -------------------------------
     * Steps to load elf file for 32 bit mode
     * Validate elf header
     * allocate enough ram for entire elf file (Zeroed)
     * Load the program header which is elf start + the proggram header offset
     * This header is the first in an array so loop though each using the number of program headers gained form the elf header
     *  if the header type is LOAD then
     *      make sure reported sizes make seance
     *      move section from offset of file to offset from allocated mem
     *      add Read write execute flags to memory (flat memory design so no worrys)
     *
     *Next go through each Section in each section header table checking for the main function in sections of type dynsym aka dynamic symbol
     *  to check if the symbol is found the offset of the name is added to the string table address(the section of which will be linked to by this section).
     *  if the symbol is found
     *      the function pointer to call is the start of the allocated memory + the offset from the symbol header
     *
     *Next go through each relocatable section
     *  for each entry(th count of which is the size of the section div the size of a relocatable)
     *     !!!!!!!!!NOT CALCULATED!!!!!!
     *         relocate function
     *
     *Finally Call the entry point passing three prams int argcount, char ** argvals, and char ** environment prams
     *
     */
    void * load32Elf(Elf32Header* header);
	/**
     * Checks if my elf loader can handle the passed elf file
     * @param header header of the elf to load
     * @return If the elf loader can handel the elf
     */
	bool checkValidaty32bit(Elf32Header* header);
	/**
     * Loads elf sections into memory and executes them
     * @param header the location in which to find the elf file in memory.
     * @param argc the number of arguments
     * @param argv a pointer to the arguments to pass to the elf file
     * @param envp a pointer to the environment pramaters
     * @return The result of the elf file, or -247 if the elf could not be loaded
     */
	int loadElf(void* header, int argc,char** argv, char** envp);
	/**
     * Returns a pointer to the entry point or null
     * @param ptr pointer to the start of the elf file in memory
     * @return a pointer to the entry point of the loaded elf file ready for execution
     */
	void* loadElf(void* ptr);
	/**
     * Checks if header is valid and if so returns the type which the header is
     * @param ptr the pointer to the header to validate
     * @return A value which refers to if the header is valid, x86 or x64
     */
    unsigned char headerType(void* ptr);

}
#endif


