#include <stdint.h>


#ifndef EXCECPTION
#define EXCECPTION
#include "../headers/pic.h"
#include "../headers/exception_consts.h"
namespace idt{
	const uint16_t MAX_ENTRIES = 256;//15 entries
	#define IDT_TYPE_32BIT_TRAP 0xf
	#define IDT_TYPE_32BIT_TASK 0x5
	#define IDT_TYPE_32BIT_INTERRUPT 0xE
	#define IDT_TYPE_16BIT_INTERRUPT 0x6
	#define IDT_TYPE_16BIT_TRAP 0x7


	/**
	 * Defines an IDT entry
	 */
	struct idt_entry
	{
	    unsigned int base_lo : 16;
	    unsigned int sel : 16;        /* Our kernel code segment goes here! */
	    unsigned int unused:8;     /* This will ALWAYS be set to 0! */
	    unsigned int type:4;
	    unsigned int always0:1;
	    unsigned int ring:2;//min ring level
	    unsigned int present:1;
	    /* Set using the above table! */
	    unsigned int base_hi:16;
	} __attribute__((packed));
	/**
	 * Defines the pointer which points to the IDT
	 */
	struct idt_ptr
	{
	    unsigned short size;
	    unsigned int start;
	} __attribute__((packed));

	/**
	 * Declare an IDT of 256 entries.
	 */
	static struct idt_entry idt[MAX_ENTRIES];
	/**
	 * Declare the object which will pointer to the IDT
	 */
	static struct idt_ptr idtp;////

	/* This exists in 'start.asm', and is used to load our IDT */
	//extern void idt_load();

	/**
	 * Used to add entries to the IDT
	 * @param num interupt id
	 * @param entry IDT entry to insert
	 * @return true if entry inserted successfully
	 */
	bool add_exception_handler(unsigned int num, idt_entry entry);
	/**
	 * Initalises the Interupt descripter table
	 */
	void init_exception_handler();
	/**
	 * Initalises the Interupt descripter table with the given pointer to the IDT
	 * @param idt_ref ponter object which refers to the IDT
	 */
	void loadIdt(struct idt_ptr* idt_ref);
	/**
	 * Updates the pointer held in an IDT entry to the pointer passed
	 * @param function new function pointer
	 * @param entry IDT entry to update
	 */
	void calculateAddress(void (*function)(),idt_entry* entry);
	/**
	 * Adds the relevent entrys to the IDT
	 * Then loads the IDT
	 */
	void setUpIdt();
	

	/**
	 * Structure of the stack after the assembly handeler has set it up
	 * this is passed to the C/ C++ handler
	 * from http://www.osdever.net/bkerndev/Docs/isrs.htm
	 */
	struct regs
	{
	    unsigned int gs, fs, es, ds;      /* pushed the segs last */
	    unsigned int edi, esi, ebp, esp, ebx, edx, ecx, eax;  /* pushed by 'pusha' */
	    unsigned int int_no, err_code;    /* our 'push byte #' and ecodes do this */
	    unsigned int eip, cs, eflags, useresp, ss;   /* pushed by the processor automatically */ 
	};
}

/**
 * Refreances to the handlers for teh interupts
 * These are written in assembly
 */
	extern "C"{
		
	
		void divide_error();
		void debug();
		void nmi();
		void int3();
		void overflow();
		void bounds();
		void invalid_op();
		void device_not_available();
		void double_fault();
		void coprocessor_segment_overrun();
		void invalid_TSS();
		void segment_not_present();
		void stack_segment();
		void general_protection();
		void page_fault();
		void unknown_interupt();
		void coprocessor_error();
		void alignment_check();
		void machine_check();
		void simd_coprocessor_error();
		void reserved();
		
		void irq0();
		void irq1();
		void irq2();
		void irq3();
		void irq4();
		void irq5();
		void irq6();
		void irq7();
		void irq8();
		void irq9();
		void irq10();
		void irq11();
		void irq12();
		void irq13();
		void irq14();
		void irq15();
		
		void test_sys_call();
	}
#endif

