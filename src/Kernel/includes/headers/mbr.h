#ifndef MBR
#define MBR
#define MBR_MAXENTRIES

namespace mbr{
 	/**
 	 * Structure of an MBR entry
 	 */
    struct mbr_entry{
    	unsigned char bootable;
	unsigned char CHS_start[3];
	unsigned char type;
	unsigned char CHS_end[3];
	unsigned long LBA_start;	
	unsigned long size;
    }__attribute__ ((packed));

	/**
      * Simple function to use the MBR to return a pointer to the start of the kernel.
      * @return a pointer to the start of the kernel.
      */
    void* getKernelStart();
	/**
	* Uses the MBR to reveal how large the  MBR is.
	* @return the size of the kernel.
	*/
	unsigned long getKernelSize();
	/**
	 * Simple function to use the MBR to return a pointer to the start of the Elf file.
	 * @return a pointer to the start of the Elf.
	 */
	void* getElfStart();
	/**
	 * Uses the MBR to reveal how large the test Elf file is.
	 * @return size of the test Elf file.
	 */
	unsigned long getElfSize();
}
#endif
