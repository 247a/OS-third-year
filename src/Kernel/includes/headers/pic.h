#include "../headers/ports.h"
#ifndef PIC
#define PIC
namespace pic{
	#define IRQ0 32
	#define IRQ1 33
	#define IRQ2 34
	#define IRQ3 35
	#define IRQ4 36
	#define IRQ5 37
	#define IRQ6 38
	#define IRQ7 39
	#define IRQ8 40
	#define IRQ9 41
	#define IRQ10 42
	#define IRQ11 43
	#define IRQ12 44
	#define IRQ13 45
	#define IRQ14 46
	#define IRQ15 47
	
	#define bestPicOffsetBase 0x20
	/**
	 * Responcible for changeing the offset of the IRQs
	 * @pram offset offset into the IDT for the pics interupts
	 */
	void remap(char offset);
	/**
	 * Responcible for changeing the offset of the IRQs
	 * @pram MasterOffset the offset into the IDT for the master pic's interupts
	 * @pram SlaveOffset the offset into the IDT for the slave pic's interupts
	 */

	void remap(char MasterOffset,char SlaveOffset);

	/**
	 * Responcible for letting the PICs know the interupt handler
	 * 		has finished with that IRQ and to send the next one
	 * @pram IRQnum id of the interupt handeled
	 */
	void interurptComplete(char IRQnum);
}
#endif
