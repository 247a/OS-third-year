//C FILE
#include <stdint.h>

#ifndef SCREEN
#define SCREEN
/**
 * The namespace responcible for printing to the screen
 */
namespace screen{
	/**
	 * The list of avilible colours and the relevent values
	 */
	enum consoleColor{
		black=0x0,
		blue=0x1,
		green=0x2,
		cyan=0x3,
		red=0x4,
		magenta=0x5,
		brown=0x6,
		lightGrey=0x7,
		darkGrey=0x8,
		lightBlue=0x9,
		lightGreen=0xa,
		lightCyan=0xb,
		lightRed=0xc,
		lightMagenta=0xd,
		lightBrown=0xe,
		white=0xf
	};
	/**
	 * Sets the current posistion on the screen to continue printing from
	 * @param newX the column to print from
	 * @param newY the row to print from
	 */
   	void setXY(uint8_t newX, uint8_t newY);
	/**
	 * Converts the colour enum to the value which the GPU expects
	 * @param forGound The enum value for the foreground
	 * @param backGround the enum value for the background
	 * @return the value which is the combination of the two colour enums in the form the GPU expects
	 */
	uint8_t screenMakeAttributeByte(enum consoleColor forGound, enum consoleColor backGround);
	/**
	 * Pints a string to the console in a requested color followed by a new line
	 * @param message ponter to the message to write
	 * @param size the number of charators to print
	 * @param forGound color of the foreground
	 * @param backGround color of the background
	 */
	void printl(char* message, int size, enum consoleColor forGound, enum consoleColor backGround);
	/**
	 * Pints a string to the console in a requested color
	 * @param message ponter to the message to write
	 * @param size the number of charators to print
	 * @param forGound color of the foreground
	 * @param backGround color of the background
	 */
	void print(char* message, int size,enum consoleColor forGound, enum consoleColor backGround);
	/**
	 * Writes a new line to the screen in the requested color
	 * The color is i the form the GPU expects
	 * @param attributeByte the color of the newline in the form the GPU expects
	 */
	void newLine(uint8_t attributeByte);
	/**
	 * Clears all text from the screen, and sets the color to white text on black background
	 */
	void clearScreen();
	/**
	 * Prints a raw memory rejion to the screen
	 * @param ptr pointer to the memory region to print
	 * @param size amount of the rejoin to print
	 */
	void printMem(char* ptr, int size);
	/**
	*Converts the 4bits passed to hex and prints the hex to screen
	*As a safty mecanisem it masks to 0x0F so that if the top four bits are set no unexpected behavior will occure.
	*
	*@deprecated: it is unlikely this is the function that calling code should call
	*		it is likely callers are looking for printHexChar(char,consoleColor,consoleColor)
	*		unless they are from this namespace
	*@pram digit: the bottom four bits of this value will be converted to hex and printed to the screen
	*@pram screen_index: the location on the screen to print to
	*@pram attributeByte: the color of the caractor to print to the screen
	*/
	void printHexDigit(char digit, uint16_t screen_index, uint8_t attributeByte);
	/**
	*Prints the hex of a given char to the screen in a given color
	*@pram digit: the charactor to convert to hex and show on screen
	*@pram forGound: the color of the charator
	*@pram backGround: the color of the background for the charator
	*/
	void printHexChar(char digit, enum consoleColor forGound, enum consoleColor backGround);
}
#undef videoRamPointer
#endif
