#ifndef __MEMORY__
#define __MEMORY__
#define NULL 0

struct memoryHeader{
    memoryHeader* prevBlock;
    memoryHeader* nextBlock;
    int size;
};
extern const void* chainStart;

//set for test
extern const void* maxMem;
/**
 * Allocates the specified number of bytes and returns a pointer to the allocated region
 * @param size number of bytes to allocate
 * @return Return pointer to allocated memory, or null if not possible
 */
void* malloc(unsigned int size);
/**
 * Frees the memory pointed at by the given pointer
 * @param ptr pointer to the memory block to free
 */
void free(void* ptr);
/**
 * @hide
 */
void* realloc (void* ptr, unsigned int  size);
/**
 * @hide
 */
void* calloc(unsigned int  num, unsigned int  size);
/**
 * @hide
 */
void memcp(void* src, void* dest, int size);
/**
 *Copies number of bytes define in size from the memory at src to the memory at dest so that dest contains exactaly what src did even if the addresses over lap.
 * @param src source address
 * @param dest destination address
 * @param size number of bytes to move
 * @note could have gone down the route of allocate additional mem copy to temp location then copy back
 * however this way is faster and can handle very large memory sizes wich the other method can't
 */
void memmove(void* src, void* dest, int size);
/**
 * Sets a defined number of bytes at the given memory address to the given value
 * @param dest Address of memory to set
 * @param val value to set memory to
 * @param size number of bytes to set
 */
void memset(void* dest, char val, int size);
/**
 * Compares two C style strings and retuns if they are the same
 * @param str1 First string to compare
 * @param str2 Second string to compare
 * @return zero if both strings are the same,
 * >0 if the first diffeing char is larger in str1,
 * <0 if the first differing char is larger in str2
 * @note based on http://www.cplusplus.com/reference/cstring/strcmp/
 */
int strcmp(const char * str1, const char * str2 );
#endif
