//using defines to try to reduce runtime load (size and proccessing)

//===================
//32bit or 64 bit
//===================
#define ELF_32BIT_INVALID 0
#define ELF_32BIT_32BIT 1
#define ELF_32BIT_64BIT 2
//===================
//edianness	
//===================
#define ELF_ENDIAN_LITTLE 1
#define ELF_ENDIAN_BIG 2
	
//===================
//Execution Type
//===================
#define ELF_EXECUTION_TYPE_RELOCATABLE 1
#define ELF_EXECUTION_TYPE_EXECUTABLE 2
#define ELF_EXECUTION_TYPE_SHARED 3
#define ELF_EXECUTION_TYPE_CORE 4

//==================
//elf version
//==================
#define ELF_ELF_VERSION_CURRENTLY_SUPPORTED 1

//==============================
//Target system instruction type
//==============================
#define ELF_INSTRUCTION_NA 0x00
//pc
#define ELF_INSTRUCTION_x86 0x03
//raspberrypi is based on this
#define ELF_INSTRUCTION_ARM 0x28
//pc 64bit
#define ELF_INSTRUCTION_IA_64 0x32
//pc 64bit
#define ELF_INSTRUCTION_X86_64 0x3E
//raspberrypi3 on 64bit is based on this
#define ELF_INSTRUCTION_AArch64 0xB7

//===================
//Section types
//===================
#define ELF_SECTION_RELOCATIABLE 9
#define ELF_SECTION_DYNAMIC_SYMBOLS	  11