#ifndef RINGSWITCH
#define RINGSWITCH
namespace ringSwitch{
    /**
	 * Switchs the CPU to userland
	 * then calls the passed function pointer
	 * on the return of the user function halt the cpu
	 * @param userfunction user function to call after cpu switches to ring 3
	 */
void enter_usermode (void (*userfunction)());
    /**
     * @hide
     */
void enter_usermode2 (void (*userfunction)());
}
#endif
