[bits 32]
[extern kernelmain]
[global _start]
[global _ring3]
[global _installTss]
[extern _init]
;====================================================================
; Kernel memory starts
; This loads C module	 
;====================================================================
_start:
	  ;Create new stack
  	mov ebp, 0x90000 	;new base pointer
  	mov esp,ebp 		;make stack empty
    call _init ;call c global constructors(disabled as causes triple fault)
  	call kernelmain
;===================================================================
;Hang
;===================================================================
_hang:
	jmp _hang
;===================================================================
;Move to ring 3 aka userland
;===================================================================
_ring3:
cli;clear interupts temperarally
mov ax, 0x1b;user mode data selector is 0x18 (GDT entry 3). Also sets RPL to 3
mov ds, ax
mov es, ax
mov fs, ax
mov gs, ax

mov eax, esp;Store stack pointer(saves fixing later)
push 0x1b;SS, notice it uses same selector as above
push eax; save old stack pointer on stack 
pushf; Save Flags

;Source for Bug fix https://web.archive.org/web/20160326062442/http://jamesmolloy.co.uk/tutorial_html/10.-User%20Mode.html
pop eax ; Get EFLAGS back into EAX. The only way to read EFLAGS is to pushf then pop.
or eax, 0x200 ; Set the IF flag. AKA enable interupts
push eax ; Push the new EFLAGS value back onto the stack.

push 0x23;CS, user mode code selector is 0x20. With RPL 3 this is 0x23
push .return;push pointer to lable 1 searching forward in memory
iret
.return:
ret
;======================================================================
;Install the Task State Segment
;======================================================================
_installTss:
   mov ax, 0x2B      ; Load the index of our TSS structure - The index is
                     ; 0x28, as it is the 5th selector and each is 8 bytes
                     ; long, but we set the bottom two bits (making 0x2B)
                     ; so that it has an RPL of 3, not zero.
   ltr ax            ; Load 0x2B into the task state register.
   ret
