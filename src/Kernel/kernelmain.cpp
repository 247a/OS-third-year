#include "includes/headers/screen.h"
#include "includes/headers/gdt.h"
#include "includes/headers/ringSwitch.h"
#include "includes/headers/idt.h"
#include "includes/headers/mbr.h"
#include "includes/headers/elf.h"
#include "includes/headers/memory.h"
/**
 * Kernel namespace is reserved for the main method for the kernel
 * Utility methods are held in their own respective namespace
 */
namespace kernel{
	/**
	 * Main kernel method called after the system has been setup
	 */
	void main();
	/**
	 * Function to call after moving into userland
	 */
	void userland_function();
	extern "C"{ /* Use C linkage for kernel_main. */
		/**
		 * Main entry point to the kernel from the
		 * assembly wrapper
		 */
		void kernelmain() {
			//Set up the Global descriptor table first as that is the main memory map
			gdt::SetupNewGDT();
			//Then set up the Interupt handlers and remap the PIC
			idt::setUpIdt();
			//Complete other steps
			kernel::main();

			//Just stop
			for(;;) {
			    asm("hlt");
			}   
		}
	}

	/**
	 * Function to call after moving into userland
	 */
	void userland_function(){
		char UserLoadMsg[] = "Userland loaded successfully";
		screen::printl(UserLoadMsg,sizeof(UserLoadMsg),screen::white,screen::black);

		//sleep as not been instructed to exit
		for(;;) {}  
	}
	/**
	 * Main kernel method called after the system has been setup
	 */
	void main(){
		screen::clearScreen();
		char KernelLoadMsg[] = "The kernel has been sucessfully loaded";
		screen::printl(KernelLoadMsg,sizeof(KernelLoadMsg),screen::white,screen::black);
		void * ptr = malloc(2);
		void * ptr2 = malloc(5);
		elf::printElfInfo(mbr::getElfStart());
		ringSwitch::enter_usermode(userland_function);
	}
}

