#include "../headers/screen.h"
#include "../headers/memory.h"
namespace screen {
	//Cursor Y pos
	uint8_t y = 0;
	//Cursor X pos
	uint8_t x = 0;

	

	const uint8_t max_y = 25;
	const uint8_t max_x = 80;

	void setXY(uint8_t newX, uint8_t newY) {
		if (newX<max_x && newY<max_y) {
			x = newX;
			y = newY;
		}
	}

	uint8_t screenMakeAttributeByte(enum consoleColor forGound, enum consoleColor backGround) {
		return forGound | backGround << 4;
	}

	void printl(char* message, int size, enum consoleColor forGound, enum consoleColor backGround) {
		print(message, size, forGound, backGround);//use prewritten print function
        uint8_t attributeByte = screenMakeAttributeByte(forGound, backGround);
        x=0;
        if (y == max_y-1) {//if on end line add newline
			newLine(attributeByte);
		}
		else {//else just move to next line
		    y++;
		}
	}

	void print(char* message, int size, enum consoleColor forGound, enum consoleColor backGround) {
        char* videoRamPointer = (char*)0xb8000;
		uint16_t screen_index = ((y * max_x) + x) * 2; //Jump to current position
		uint8_t attributeByte = screenMakeAttributeByte(forGound, backGround);
        bool test = false;
		for (int message_index = 0; message_index < size; message_index++  && x++) {
			// set character

			videoRamPointer[screen_index] = message[message_index];
			//attribute-byte reset
			videoRamPointer[screen_index+1] = attributeByte;

		
			if (x == max_x) {//if at end of line
				x = 0;//move cursor to start of line
				if (y == max_y-1) {//if on end line add newline
					newLine(attributeByte);
					screen_index -= max_x * 2;
                    test=true;
				}
				else {//else just move to next line
					y++;
                    screen_index += 2;
				}
			}else{
                screen_index += 2;
            }
		}
	}

	void newLine(uint8_t attributeByte) {
char* videoRamPointer = (char*)0xb8000;
		//Clear new bottom line
		uint16_t i = max_x * (max_y-1) * 2;//Skip over till last line
		//Shift up one line
		memcp(videoRamPointer+(max_x*2), videoRamPointer, i);

		while (i < max_x * max_y * 2) {
			// blank character
			videoRamPointer[i] = ' ';
			i++;
			//attribute-byte reset
			videoRamPointer[i] = attributeByte;
			i++;
		}
	}

	void clearScreen() {
char* videoRamPointer = (char*)0xb8000;

		uint16_t i = 0;
		uint8_t attributeByte = screenMakeAttributeByte(white, black);
		while (i < max_x * max_y * 2) {
			// blank character
			videoRamPointer[i] = ' ';
			i++;
			//attribute-byte reset
			videoRamPointer[i] = attributeByte;
			i++;
		}
	}
	
	//Converted asm print reg to c
	void printMem(char* ptr, int size) {
		int ii = 0;
		uint16_t screen_index = y * x * 2; //Jump to current position
		uint8_t attributeByte = screenMakeAttributeByte(white, black);
		while ((screen_index < ((max_x * max_y) - 2) * 2) && ii < size) {
			char mem = (ptr[ii] & 0xF0) >> 4;
			printHexDigit(mem, screen_index, attributeByte);
			screen_index + 2;

			x++;
			if (x == max_x) {//if at end of line
				x = 0;//move cursor to start of line
				if (y == max_y - 1) {//if on end line add newline
					newLine(attributeByte);
					screen_index -= max_x * 2;
				}
				else {//else just move to next line
					y++;
				}
			}

			mem = (ptr[ii] & 0x0F);
			printHexDigit(mem, screen_index, attributeByte);
			screen_index + 2;

			x++;
			if (x == max_x) {//if at end of line
				x = 0;//move cursor to start of line
				if (y == max_y - 1) {//if on end line add newline
					newLine(attributeByte);
					screen_index -= max_x * 2;
				}
				else {//else just move to next line
					y++;
				}
			}

			ii++;
		}
	}

	void printHexDigit(char digit, uint16_t screen_index, uint8_t attributeByte) {
        char* videoRamPointer = (char*)0xb8000;
		if (digit >= 0x0A) {
			digit += 7;
		}
		digit += 48;
		videoRamPointer[screen_index] = digit;
		//attribute-byte reset
		videoRamPointer[screen_index++] = attributeByte;
	}
}
