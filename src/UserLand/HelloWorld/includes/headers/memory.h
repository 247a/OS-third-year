#ifndef __MEMORY__
#define __MEMORY__
#define NULL 0

struct memoryHeader_s{
    memoryHeader_s* prevBlock;
    memoryHeader_s* nextBlock;
    int size;
};
extern const void** chainStart;
typedef struct memoryHeader_s memoryHeader;

//set for test
extern const void* maxMem;

void* malloc(int size);
void free(void* ptr);
void* realloc (void* ptr, int size);
void* calloc(int num, int size);

void memcp(void* src, void* dest, int size);
#endif
