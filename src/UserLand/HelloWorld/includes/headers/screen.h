//C FILE
#include <stdint.h>

#ifndef __SCREEN__
#define __SCREEN__
namespace screen{
    enum consoleColor{
    black=0x0,
    blue=0x1,
    green=0x2,
    cyan=0x3,
    red=0x4,
    magenta=0x5,
    brown=0x6,
    lightGrey=0x7,
    darkGrey=0x8,
    lightBlue=0x9,
    lightGreen=0xa,
    lightCyan=0xb,
    lightRed=0xc,
    lightMagenta=0xd,
    lightBrown=0xe,
    white=0xf
    };



   
    uint8_t screenMakeAttributeByte(enum consoleColor forGound, enum consoleColor backGround);
    void printl(char* message, int size, enum consoleColor forGound, enum consoleColor backGround);
    void print(char* message, int size,enum consoleColor forGound, enum consoleColor backGround);
    void newLine(uint8_t attributeByte);
    void clearScreen();
    void printMem(char* ptr, int size);
    void printHexDigit(char digit, uint16_t screen_index, uint8_t attributeByte);
}
#undef videoRamPointer
#endif
