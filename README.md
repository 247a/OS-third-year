#Overview:

##Build Requirements:

- nasm
- gcc, make (build-essentials)
- cross compiler

##Target system:
i686-elf
	
#Cross-complier:

##Why use a cross compiler?:

The use of a cross compiler helps reduce bugs, this is because the compiler does not assume that the build target has any prebuilt libs.
Also it simplifies the commands to compile, this is because the target arch is not required.
Also it means that if the project is to change target system all that needs to change is the compiler/compiler path and small amounts of arch dependant code
			
##CrossCompiler Prerequisites:

- An Unix-like environment
- GCC (existing release you wish to replace,or one version behind)
- G++ (if building a version of GCC >= 4.8.0)
- GNU Make
- GNU Bison
- Flex
- GNU GMP
- GNU MPFR
- GNU MPC
- Texinfo
- ISL (optional)
- CLooG (optional)

- binutils [src](https://gnu.org/software/binutils/)
- gcc [src](https://gnu.org/software/gcc/)
	
##CrossCompiler Env setup:

	get additional required files
	#gcc-5.2.0/contrib/download_prerequisites
	
	set output path
	#export PREFIX="$HOME/opt/cross"
	set target arch
	#export TARGET=i686-elf
	add new crosscompiler to path
	#export PATH="$PREFIX/bin:$PATH"
	
	make binutils
	#mkdir build-binutils
	#cd build-binutils
	#../binutils-x.y.z/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
	#make
	#make install
	#cd ..
		
	Check binutils was made successfully
	#which -- $TARGET-as || echo $TARGET-as is not in the PATH
	
	build gcc
	#mkdir build-gcc
	#cd build-gcc
	#../gcc-x.y.z/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
	#make all-gcc
	#make all-target-libgcc
	#make install-gcc
	#make install-target-libgcc

##Compilation Instructions source:

[Osdev.net](http://wiki.osdev.org/GCC_Cross-Compiler)
[waybackmachine copy-2016_05_15](https://web.archive.org/web/20160515214350/http://wiki.osdev.org/GCC_Cross-Compiler)

#Bootloader

##What is a bootloader?:

A boot loader is a small amount of code which normally resides at the beginning of a harddrive. 
This code is responsible for doing several things. 
These things include loading the kernal into memory, setting up a stack and moving to protected mode.

##How does the bootloader get loaded?:
	
On machines with a bios like the one this operating system is developed the bios loads the bootloader.
Bios stands for basic input output system.
The bios loads the bootloader from the first drive in the setup boot order which contains a magic number at the end of the first sector. 
A sector is 512 bytes long and the magic number is 0xAA55 and the drive is read in littel-endian.
Whilst the bios loads the bootloader the system is in 16bit mode other wise know as real mode.

##What language can a bootloader be created in?:

Very few languages can be used at this part of the development this is because it is required to be very low level, even more so than even the language C can provide.
How ever there are still some langues which can be utilised these include binaray, assembly and C--.

I chose to use Assembly for the development of the bootloader.
This was because binary is very prone to human error and is very difficult to learn and understand.
I also chose not to use C-- as very few people use it and it is a x16/x86 only language hance meaning the code is not easily ported even though it is easier to read as it is close to C. 

This left assembly as the only language left on my list of languages which could be utilized, assembly whilst being complex can be simplified by the use of functions (made with lables, call and ret statments) and can be commented.
Wilst still being portable to a point, it is not fully portable due to the implementation of storage being dependant on the arch, the actual language and how the language is used is the same on nearly all archs.
		
##Design of the bootloader:

This bootloader is designed in a logical way which brakes down to several stages. 
How ever the design of these stages changed slightly over the time of development due to potential optimizations.

###Initial design:
####Stage one:
In the initial design the first stage was resposible for creating the very important helper functions.
these functions where reponcible for outputing to the screen and reading from the hard drive.
It also contains the Master Boot Record

The sencible route and the one i took was to develop a function to print one charactor to the screen and build from there.
This meant that i could test the print to screen code worked with set inputs and then work on making the hard drive helper functions.
And test by setting the image to have a set pattern for the second sector then read it and print it to the screen.
This was all neccessery due to the inherent difficulty of debugging assembly proggrammes which brings us onto Stage 2's design.

####Stage two:
This stage was responcible for moving the system to 32bit protected mode.

This would mean that for stage three i could then begin to use other higer languages like C and C++ for development.
Which means that i can use common debugging techniqs for these higher level languages.

####Stage three:
This stage was going to be responcible for handleing the file system and load the kernel.
At the time i was going to use FAT32, however after further resurch fears of licencing issues made me rethink that file system.
I then chose Lean file system, how ever the specification and only implimentation was still in the alpha stage and all refeances of infomation showed development had ceased adlest 3 years prior.
At this point i thought it be wise to leave desions on a file system to a later point.

###Code and Design review:
After getting to the point of finishing Stage 2.
I stumbled across the source code of, the open source bootloader, GRUB.
This bootloader is used by, the common open source os called, linux.
Although the version of GRUB that i was looking at was a legacy version it did show that i was going in a right direction in my design.
This was because the GRUB bootloader had a Stage 1 and stage 2 and it's stage one followed the same sort of rules as mine which is, to load the next stages.

Soon after this i desided to analize the output of my bootloader.
From my analisis i found that there was a lot of unused space in the first sector in which stage one resided.
And also that a lot space was unused in the second sector in which the second stage resided so i decided to tweak the design and move stage 2 into stage 1.

This meant a new design was formed

###New Design:

####Stage one:

This Stage once agine is responcible for making the functions for printing to the screen and reading from the harddrive.
But in addition to this it also has the structure for defining the memory layout of protected mode and the function to switch to protected mode.

During the restucture most of the code was not changed just the definitions and calls where moved. 
For instance the function and structure definitons were taken from one include directory and put into the other.
And to call these functions i added the same code as was in stage two after stage one had loaded the next stage into memory but before jumping to it.

This is because all the code written in 16bit real mode is useless in 32bit real mode, heance i wanted to preload the next stage to aid in development.

####Stage two:

This starts out with a very small assembly file to call the main C function for stage 2. 
Then in C i rewrite all the functions used in Stage 1 bar those used to switch to protected mode.
Following that i begin to handle a file system and load the kernal.
However before i begin work on a file system, i enfizied my work on just loading the kernal file.
I chose to load the kernal similerly to how the other stages had been loaded as then, i could tweak how that file is loaded once the file system had been finalized.

##Implemantation:

###stage one

The first stage follows my design very closely.
First it includes the screen functions, which allow printing to the screen. These are used to out put error messages, wilst some could be removed it is safer to leave in the debug functions and not call them instead of find a bug later on and have to reimplement some of the debug functions. These functions use bios interupt 0x10.
Next the harddrive read functions are included this allows for reading the next sectors into memory and calling them. These functions once agine utilise the bios but this time the interupt number is 0x13
after this there is the hang function which is required so that if the os is to stop executing for example in the case of a majior unrecoverable error, then the desigered behavior is to try to stop executeing any garbage data in ram hence the jump stament stating to jump to the current line other wise know as an infinate loop.
following this is the file defining all the strings used to output simple messages to the operator.
Next is the Global descriptor table. this file defines the memory model used in protected mode. Out of the two memory models i could use i chose the simple flat model. The reason for this is due to it's simplistic design, and also due to the recomendation for its use by multiple sources. [Intel][Osdev][nick blundle youtube]
Finally we have the code to move from realmode to protected mode[nick blundle youtube]
And finally we have the mbr. this is a simple structure which defines the partions on the disk. the MBR can contain 4 root entries and each contain a flag to show if it is the active or boot partion, a start address, a file system identifiyer, a end address, an nother start address but in a different format, and the number of sectors the partionton contains.

###stage two

This stage starts in 32 bit mode.Due to lack of a file system at design time i will be using the mbr to house the referance to the kernal. Originally this was going to be tempory as i assumed that there would not be a MBR partion type id for just raw code. However upon further inspection there seems there is, the id is 0x27 and is named RooterBoot, and it's proper usage is a kernal partition containing just an ELF kernel with no file system. how ever any user applicatons or data would not be able to fall under this id. How ever there is the id 7f which is for the Alt-Os.
A note on useing defined partion id's. Even though there are map's which say this filesystem is this id. Due to the free for all nature of desgination of the id's and the small number (01 - ff) it is common to see multiple different file systems share the same id. however to reduce conflicts and be as close to normal standerderisation my aim is to keep to predefined system ids.

Also to be able to read from the harddrive or any other hardware additional infomation is required from the bios but this infomation is stored in ram. hence it is possible to crate a memory map of the table of infomation and refer to it when needed, for instance when writing the hdd driver.
stage two is written i C and is compiled with the cross compiler build in an eralyer section.
but to be able to enter the C code a tiny wrapper is needed which is simpily a jump statment to an external lable which is in C format.

##Bootloader C code
###Screen driver
Once in the loaded C code i begin creating the simple driver to print to the screen. This is due to the fact that it is no longer possible to use any of the old bios routines with out switching back to 16bit real mode. However to do this is highly inefficant, therefor most if not all sources of my research states if possible avoid switching back to 16bit mode. 

During this i discovered that the command i was using to link all the c code together was slightly incorrect. This was a problem as every time i made a global variable the linker would complaine about data and text sections of the code overlapping. This is because the global variables need to go into the data section of the binary, which cannot be the same location as the read only code. After this i extracted the linker infomation from the make file into a seprate linker file, this meant that it was easier to read and modify if changes where required later.

The screen driver is once agine the first thing implemented as this alows for static debugging. where we print to the screen the content of what we wish to test. This is not the best form of debugging but as a file system driver has not yet been implemented it is not possible tto make a running log, but is one of the main ways other os's like linux debug thier kernals(http://wiki.osdev.org/Kernel_Debugging,  http://elinux.org/Kernel_Debugging_Tips). Also i have yet to get a sutible amount of C code to test that gdb will successfuly connect and cause a breakpoint and return approptriate values. 

The way in which the screen driver operates is useing a memory mapped io model. This means that instead of communicating with the device directly we just place data in a set location in memory and when the device is ready it will read the allocated area of memory and interact with it acordingly. When the gpu is in text mode (like it was when the bios was acting on it) the memory is ethier at 0xB8000 for a colour monitor or 0xB0000 for a monocrome monitor. These days the mojority of monitors which are still in use are the colour variaty, this is also the variaty i have and will be developing aginst. To detect the sort of monitor present we need to query the bios, there are two ways to do this the first being whilst in 16bit mode use the bios call ??????? or in any mode query the bios data area in memory. Directly querying the bios memory is not recommended however as the memory map for this area is not fully standerdized and therefor some bioses may store data at different offsets.

###After screen Driver
I was initially planning on creating the what i initially thought would be the next logical step which was the hdd driver which religed on a pci driver. but during the initial resurch i realised a beeter aproach would be to start makeing a memory manager. this means that the data loaded would not colide with any other data hence saveing headaches in the forceable future. This also means that i can begin to use a standerd c library to add fuctionalilidy like strings and vectors, which will aid in development.

###Memory manager
The C Specification defines a couple of functions these are: calloc, free, malloc, realloc. Out of these the core functions are malloc and free, these are the allocation and deallocation functions respectivly. Whilst the calloc is simmiler to malloc but initializes the memory to zero instead of just leaveing the old data in the alocated space and realloc moves a block of memory to another location in ram. Sources [ cstdlib,](http://www.cplusplus.com/reference/cstdlib/)[ osdev,](http://wiki.osdev.org/Writing_a_memory_manager)

####design

Before jumping in to write code the frist step in memory management creation is designing how the memory map will be layed out. For this pens and paper is required and lots of mindstorming.

This is because all the memory allocator does is finds a free block, marks it as used and returns the pointer.

There are several methods i thought of which could work each with their own advantages and disadvantages. Most of these designs work of a chain in one way or an other as that is the only real way other than a hashmap, to make the memory map.

#####Method 1

The first method consists of an intitial header which contained a pointer and a boolean, this header is permentaly stored at the start of the memory allocation area. The pointer points to the next memory location and the boolean states if it is the end of the memory map chain. 
Then each block of allocated memory begins with a header consisting of a pointer to the next location memory location, a boolean stating if it is the last entry on the memory chain and finaly a int stating the size of the memory block in bytes.

This is a very simple design. But there is no way to traverse back up the chain so on deallocation it is required to go down the chain to find the entry before and update the next memory loction pointer. This design is however good for the fact that it conseves space. And the size of the chain is dynamic.

This method can be used in two ways, the first is makeing sure that each entry points to the next linierly in memory this is good as it make it easy to see if there is free space between two memory locations and how much. This means adding entrys in the middel of the chain. The other way is having the chain list the order the entries where made, by just appending to the end of the list, this is bad as it addes overhead to seeing the free space between memory blocks.

#####Method 2

This method builds off of Method 1 to make the proccess of deallocation easier. It consists of an intitial header which contained a pointer and a boolean, this header is permentaly stored at the start of the memory allocation area. The pointer points to the next memory location and the boolean states if it is the end of the memory map chain. 
Then each block of allocated memory begins with a header consisting of a pointer to the next location memory location, a pointer to the previous location, a boolean stating if it is the last entry on the memory chain and finaly a int stating the size of the memory block in bytes.

This is again a very simple design. And has the problem discovered in the privous solution solved. This design is also good for the fact that it conseves space. And again the chain size is dynamic.

This method again can be used in two ways, the first is makeing sure that each entry points to the next linierly in memory this is good as it make it easy to see if there is free space between two memory locations and how much. This means adding entrys in the middel of the chain. The other way is having the chain list the order the entries where made, by just appending to the end of the list, this is bad as it addes overhead to seeing the free space between memory blocks.

#####Method 3

This method works on a completely different way of thinking. This method works by haveing two types of memory block, a header block and a raw data block. The raw data block is the what the application calling malloc will store. where as the header blocks are the blocks the memory manager it's self will use. These contain several peacies of infomation. 
And can easily be shown with the pudo-code:

	struct headerEntry{
		bool exists;
		char* memoryLocation;
		int alocatedSize;
	}
	struct headerBlock{
		int numEntries;
		int chainIndex;
		headerEntry prevBlock;
		headerEntry nextBlock;
		headerEntry[numEntries] entry;
    }

This is good in the fact that both the header block and the chain is dynamic in size. The block is dynamic in size as if there is free space at the end of the block the numEntries can be increased for each entry appended and the previos and next block can easily be updated with on jump either way. The main disadvantage of this approch is that it is not easy to see how much space is between each entry as entrys have no relation to each other as entries will be inserted in a first free space basis. 

#####Method 4

This method works similer to method 3 but with the pudocode:

	struct headerEntry{
		bool exists;
		char* memoryLocation;
		int alocatedSize;
    }
	struct rawEntry{
		bool exists;
		char* memoryLocation;
		char* prevMemoryLocation;
		int prevMemoryChainIndex;
		char* nextMemoryLocation;
		int nextMemoryChainIndex; //-1 if last memory block(includes raw and header blocks)
		int alocatedSize;
	}
	struct headerBlock{
		int numEntries;
		int chainIndex;
		headerEntry prevBlock;
		headerEntry nextBlock;
		rawEntry[numEntries] entry;
	}

This method shares all the benifits of the previous method but allows easy calulation of free space between blocks and also allows an easyier method of finding related entrys to update. This is however in theory still slower than method 2.

#####Method Chosen

On review of the different methods, method 2 with a linaer chain through the memory region seems to be the best. This is because it does not waste much space. It also is in theory fast in both allocation and deallocation. And it is dynamic in size. it also does not require much conversion from the pointer to the raw data and the pointer to the header, as the header pointer is always a set offet to the raw pointer. It also prevides a well known starting point for the chain.

#####Implementation issues - Error Handleing

During my time implementing this i came across two issues. To fix this i used GDB with qemu to be able to step though the code and see where things where going wrong. This required a change in how the binary was creaded in order to create debug symbols.
When i steped through the code i noticed when ever i included the memory management code it would break previously working screen printing code. This turned out to be due to how i was compiling the modules or drivers. Insted of putting the code into cpp files and just having the definations in the header files i had code in header files, so i refactored the code to fix this and also i added code to compile each cpp file and link into a single include object which then can be included the main boodloader code. This fixed the code when including the memory manager but not when calling any of the functions contained within.

After this i had issues when calling malloc in which it would go into the malloc function then continue into the next function which was the free after this it would triple fault and reset. So to find the cause of this i began by researching error handling on [osdev](http://wiki.osdev.org/Exceptions), the intel developer manual Volume 3A Chapter 6 and [brokenthorn](http://www.brokenthorn.com/Resources/OSDev15.html).(http://viralpatel.net/taj/tutorial/idt.php)
From this i could add code to output a statment stateing the cause of the error and stop the reset(as to read the error messeage).


######Interupt Descriptor Table

To be able to add some error handeling code to print what was going wrong i had to implent a Interupt Descriptor Table. This is one of two structures to handle interupts, in real mode both of these acted the same and is what we used to call the bios routines. How ever in protected mode they act slightly differently, Also both exceptions and other interupts share the same table. This table has 255 entries but for the exception handler we will only be conserned with the first 31 entries. This table is similer to the priviously implemented GDT. in that there are two base structures the first is the pointer which has a base address to the table and a limit to show how large the table is.

The next structure is each entry in the table. The Interupts can be thought as someone calling you over when your free as they want you to do something this will likely be used when interacting with hardware like mouse and keyboard. Where as the exceptions are when something has gone drasticly wrong, if the computer recives an exception when it is dealing with the firt execption a new exception, called a double-fault excepion, is raised after this if tehre are still exceiptions raised whilst dealing with the current exception, a tripel-fault is raised and, the computer is restarted automaitcly.

The structure of the table entrys starts of with the low bits of the address to jump to. after this goes the code segment, from the GDT, which the exception handeler it to run. following this is a reserved byte which is always set to 0. then a set of flags, the first is the present flag, the next two say which ring is the highest that can call the interupt, and then we have the storage segment which is normally '0'. after this we have 4 bits to say if the gate is 16bit or 32bit, which i will use 32bit, and whether it is a task gate an interrupt gate or a trap gate, so i use '1110' for 32bit interrupt gate. following this we have the high bytes of the handler functions address. To load the Interupt descriptor table the assembally command lidt is used and that takes the address ,of the first type of structor, in memory.

Whilst implementing the IDT i also thought to replace the GDT once in C code as it is likely when userland applications are added access will be required.

######Interupt Descriptor Table - issues

So whilst implementing the idt i experianced several issues. Looking back these issues where very simple however the symptoms of these issues and my inexperiance in the use and setup of the IDT i found them quite difficult to solve at the time. The first issue was that my IDT just wasn't getting called, i tried every thing i could think of and it just didn't work. I was sure all the IDT code was right but to no effect. I finally worked out that in the bootloader the IDT is disabled when switching to the protected mode, however it was never reactivated. After calling the sti assembly instruction it began to work.
How ever this lead on to a new error, I kept on getting a double fault randomly. I could tell this as the handler for the double fault clearly stated there had been a double fault on the screen. So i added some static debugging code in just to see if i could find where this random exception was occuring. It turned out to be in the final system hang(where the os goes in an infinate loop when it has no more work to do as to not start running off in to random memory) so this loop should have no problems. What was weird was it was after several cycles in this simple jmp loop where the error occured. After some digging i found that a common error is that you will get double fault(or adlest what apper to be double faults) in cases where the PIC hasn't been reprogrammed yet as you get an IRQ0
which has the typical use of the system timer, which makes sence as i was not sending any input yet it just recived these errors at repeatable times. So for the time being i disabled harware interupts but kept software interupts, so that i could discover where the issue was.
[irq0](http://www.pcguide.com/ref/mbsys/res/irq/numIRQ0-c.html)
[double fault cause](http://wiki.osdev.org/Exceptions#Double_Fault)


#####Implementation issues - Potential cause
So though my time attempting to implement the interupt descriptor table, i found that the cause may be be due to the fact that i have not yet told the c compiler what calling convention to use.

This is because of the nostdlib flag passed to the compiler. This has the effect of disabling the built-in calling conventions. 

So to fix this i had to generate 4 object files which the compiler will use. Two of the files the compiler makes, ctrbegin.o and ctrend.o, these contain internals of the clib which the compiler whishes to abstract from us.
 To interface with these two object files, i had to write two new object files, called crti.o and crtn.o. After this when linking together the c code with the assembly entry point i had to add these new object files in the correct loctions to fix the calling convention issues.

#####Implementation issues - Actuall cause
So after adding the c callig convention code. And trying to debug with GDB, which was saying i was executing add %al,(%eax). What was weird is that the add%al,(%eax) began partway through the print to screen code, which was working before. The part which gave it away was the fact that the "add" code ran on after all my code then i realised empty memory would be 0x00 in qemu, what if i have finally ren past the amount of the program i am reading from the disk. So i lokked at the outputed file and it was getting to be larger than originaly expected. So with a simpile change in the stage one code to incrase the number of sectors read, the tripel faults stopped and the code began to work. But it was a key mistake to make as it will remind myself infuture the affects caused by not loading the complete application which will lead on to application loading for userland stuff.

###Keyboard
####Pic
The pic is a programmable interupt controller. This controller is something which handels most of the devices in the computer. It is part of a network of devices which allow the cpu to talk to hardware which runs slower than it without it itself slowing down causing other processes to be affected.

There are numerous reasons why it is important to interface with the pic in a standerd computer. One of these is to make sure that the system timer does not cause any faults when it souldn't, an other is to be able to read input from the user.

To interact with the pic cpu ports are utilized this relates to the out and in assebly commands. There are various variants of this command depending on the data size like outb would output a specified byte to a specified port. The port that the pic resides is 0x20 otherwise known as port 30. To make driver constructsion simpler i wrote a simple wrapper around the port commands so that the main algorithem for the drivers can be written in C and C++ rather than assembely.

Whilst this may add overhead on low optimizations on higher optimizations the compiler should be able to make the code as efficant as if it was written in pure assbly if not faster.

Whilst the port code is usefull for reprogramming the pic it can also add fuctionality to the prexisting screen driver. This new fuctionality is control over the blinking cursor.

####Keyboard potientail implementation method 1
the first way to implement the keyboard driver would be to poll the releven cpu ports to check for data and if data is avalible read it.
Whilst this has the benifit of being a very simple implementation in some instances it is a very difficult to implemet when you are only utilising a single proccess as the OS is currently as not task sedular has been developed yet.
It is also a bad option due to the fact that it is very inefficant as the cpu will have to constantly check if the data is there, this time checking is there is data avilible would be better served doing another task, even if that task was to just sleep.

####Keyboard potentaial implemtation method 2
This second method utilises the fact that the pic sends the CPU hardware interupts, how ever these IRQs (or Interupt Requests) in there default state collide with the software interupts. The method to fix this is to remap the pic which is a common aproch in Operating System development. This then allows for an interupt handler which handles when a key is pressed, this has the advantage of simpler easier to understand code, aswell as the removeal of the need of the cpu to constantly check if a key has been pressed as it will be alerted when a key event occurs. This is the method i will ustilise as it means the implementation would not need to drasticly change if multi threading gets implmented, although undoudtedly some changes would still be required.

####Pic remaping
Remapping of the PICs IRQs as previously stated is required as otherwise there will be an overlap of the normal error handelers for things like doublefaults and the handlers for the IRQs like the system clock or the keyboard.
Remapping of the PIC heance allows the use of the pic to simplify driver development for things like keyboards as well as hard drives.

Whilst in most cases you can think of the PIC as just one entity when directly accessing it you have to realise the pic is actually two devices a master and a slave. This is important to note when setting up the remapping as well as handeling the IRQs as at the end of the IRQ you have to notify the master and slave pic that the IRQ has been proccesed and to send the next one.

The remapping of the PIC is accomplished by useing four cpu ports a data port and a command port for each of the two parts of the PIC. The ports will be able to push one byte of data at a time as that is the bandwidth od the PIC.

The process of remapping the pic can also be called initalising the pics as you say where to map the IRQs. The pics will first contain a mask of the interupts that it currently ignors (one being ignore zero being listen to the interupt) on their data ports, this mask is one byte each. So there are two ways to handle this the first is to back up the old mask and return it after initalisation. This is ok but the bios may have disabled some IRQs for what ever reason it feels. This is why there is a second method which just means you set the masks to zero after initalisation hence activating all IRQs. 

One key note with the mask is that even when the IRQ is disabled via the mask it is still fired just the pic decides not to forward that to the cpu.

So after we have decided if we will keep the old mask the next thing to start the initailsation is to send the command 0x11 to both the slave and the master on their command port.
 This indicated to the PICS that when initalising the pic should expect to recive ICW4 during this initialization(as the last bit was high). Also this indicated that there is more than one PIC in the system (as the second to last bit was low). The fourth bit from the right tells the PIC the logic trigger mode (being high would be level trigger) so we told the PIC we are in Edge Triggered mode. and the fifth bit from the right told the PIC that we are initialising it. This stage is is known as ICW1 or Initialization Control Word 1.[broken thorn](http://www.brokenthorn.com/Resources/OSDevPic.html) 
We wait a littel time between each output to the PICs to give them chance to respond, as there is no alert or flag to indicat the PICs have recived the data.

Following this we send the offset to the master and slaves data port. We add eight to the offset of the slave so that the master and slave dont collide in the IDT as the master PIC has eight interupts. A sencible value for this is 0x20 and 0x28 as that then maps the IRQs to the end of the reseved interupts and then intuern gives us the rest of the IDT for syscalls. This stage is called ICW2.

Due to telling the pic in ICW1 that there is more than one pic we now enter ICW3. This is where we tell the PICs the configuration they are in. For this we send on the Master pics data port 0x04 to indicate that there is a slave pic and on which IRQ the slave is connected. Whilst some computers may not be in this configuration the majoirty of the computers seen in modern computing will contain one PIC which is contained in the CPU and another on the motherboard. It is possible to have as many as 8 pics in a system in which case this value would be 0x0F. This is stander as "The 80x86 architecture uses IRQ line 2 to connect the master PIC to the slave PIC."[broken thorn](http://www.brokenthorn.com/Resources/OSDevPic.html).
is 
Next we tell the Slave which IRQ line is connected to the master. This is the same IRQ line, however due to the slave only being able to connect to one master it is formatted slightly different. only the three lest significant bits are used so to indicate which line is used we don't flip the bit at that position in the 8 bits avilible. we set the last three bits to that number in banary notation so IRQ3 would be 00000011 instead of 00000100. Hence as we are connected to the master via IRQ2 we use the value 0x02.

Finally due to telling the PICs during ICW1 that it would get an ICW4 we have to send an other 8bits to both the master data port and the slave data port. This allows for some final configuration like weather to auto acknolage IRQs and whether to run in buffered mode. The only option for the time being i will enable is the 80x86 mode so i send 0x01 to both ports. This now means the PIC is initalized for my use and any IRQs recived will not collide with other entries in the IDT.

Then we tidy up by issueing OCW1 or operational control 1 on the master and slave data lines to give the mask of enabled interupts (which if we backed up initally we would restor other wise we would set to zero to indicate to enable all interupts)

###User land applications
####Setup
After reimplementing the GDT in C it was neccery to add more entries for use in other cpu ring levels than ring 0 aka kernel mode.
To accomplish this all i had to do was copy the code and data segment and update the protecton level bits to indicate the ring which it may be used.

####Logic behind the switch
The way in which the cpu is swithed to a different mode(or adlest from ring 0 into another ring, an exception is raised the other way)
 is affecting how the assembly opcode iret function returns. This sounds like  complex menuver however all this entails is altering the data in set locations as to go to the userland code, and set the requested ring leavel.

####Performing the switch to userland
To accomplish this we first clear the interupts as to not get any unexpected errors or interupts whilst dealing with this sencitive state.
Next we set the AX register.
The format of the AX register is the right most two bits are the requested ring level in this case 0x03 or 11b.
The Left most bits are the offset of the userland data segment. This can be calulated simpily by 8*gdt_index.
So for the third gdt index and ring 3 we would set ax to 0x1b or 27. This is because 3*8 = 0x18 or 24 add the 3 for the ring level makes 0x1b or 27
We did this as we cannot write to the segment registers directally, and we need to overwrite the data segment(ds) and the extra segments(ES, FS, GS) with this new value.

After this is is to set up the stack which the IRET fuction will read.
For this we first push AX to the stack, Then we push the pointer to the top of the stack.
Following this we push the contents of the CPU flags.
Next we follow the same steps as before when filling the AX register but insted of using the data segment's offset we use the code segmetns offset.
The next step is to push a poniter to where we whish to end up.
 Finally we call iretd.

During my time writing this i came across an interesting issue, when attempting to test the userland modewith a fake fucntion which printed to the screen and then called cli (which should have cause a crash) i just had a working system, no crash. After looking at gdb asmbler it turned out that GCC had decided to replace my inline asm wich was flagged as volitile with a simple jmp statment which completly changed the porpose. to fix this i had to add the inline asm to the seprate assembly file and compile it separately, after this i successfully crashed.


####Uses of this other than just userland applications
Athough the main reason which we utilized this method is to allow userland applicaions which do not require full control of the hardware and instead should call the kernel functions through interups.
It can be used to enable a mode called v8086 mode. What this means is if we set bit 17 in the flags before pushing them to the stack it is possible to run 16bit code even though we have moved to protected mode.
This in turn means it is possible to bios calls once more like the bios call to read from the harddrive. This is however not recommened as this would put the IRQ handlers into a bad state, it may freeze the cpu untill it recives a reponce from the hdd.

####After moving to userland
Before implementing a method in which i can return from userland code i made a simple elf file which will use the tested method to write to the screen as i know this works in userland. this elf will just print a message to state it has loaded. For testing i will just append the elf to the end of the kernel and add a new asm file with a lable so that i can see where the end of the kernel is, and hence use it to load the elf file.

To acctually call the elf when loaded i will just have to make a function pointer to a fixed addess (calcualted from the elf header) and call the function pointer. This will mean that once the application finishes then execution is returned to the os.

(Call function pointer from mem address)[http://stackoverflow.com/questions/8915797/calling-a-function-through-its-address-in-memory-in-c-c]

###Update MBR
Due to the fact that their is no filesystem implemented yet, i had to have some method to read the elf file and know where in memory it was loaded. When trying to use a fixed lable inlcuded at the end of the kernel the lable did not quite point to the correct location.
The best work around to fixe this is the MBR which is a structure at the end of the boot loader. This stucture shows the partitions on the drive, the type of partion, how large the partion is, where it starts and if it is the main partionion ak the active partion.
In the bootloader there are 4 MBR entries. More can be added via an linked list style structure by adding an entry with the systme type of extended mbr. but due to the need for only two partitions this structure is not required.

The first partionion is our active partion other wise known as our kernel. To mark this as the active patiotion the first byte is set as 0x80, after this is the CHS which i will not be useing as LBA is fine for our needs and more human readable. So the next 3 bytes are zero. the byte after is the sytem type, for the kernel i will piggy back on the system type 0x8a wich is for a raw linux kernel. After this is the CHS end agine LBA is fine for us, so the next 3 bytes are zero. the next 4 bytes are are where the kernel is located, due to it being after the bootloader, it will be 1 as the bootloader is 512 bytes, if the size changes then i will have to manually update this field. Following this is 4 bytes stating the size of the kernel, For this i used python to automate the reading of the size of the kernel.bin file.

The next partion is the elf file a larger portion of this partition header is auto generated, most of the header is the same as the kernel header. To start the active byte is set as 0 the the CHS start is equal to 0. The value i for the system type is an unused type which is 0x20. then i set the CHS end to 0 again. For the LBA start i caluclate the addess with the kernel size and the alter for the size of the bootloader. And the size is calcuated from the elf file. I also use python to calculte the padding required between each partion to make them full sectors. 

After adding the Mbr i then added code to the kernel to pharse the MBR inorder to access the ELF file headers.


###Elf format
So now that we have an elf to load and our userland envionment the next step is to read the elf format.
The elf format cosists of various headers to define diffent sections of the application like data, code and how to load and call them.

The first header for the elf file starts at the very start of the file. it starts with the magic number 0x7F 0x45 0x4c 0x46 the last 3 chars of this magic number spell ELF.

###Returning to the kernel from userland applications
Wilst it is great that we can move to userland, as previously stated we cannot use this method to get back to the kernel as if we attempt to we will recive an exception, this is a security fecture as it means you cant have unprivaliged applications running privilaged applications.
So inorder to return to the kernel we are required to implement a system called the TSS. This system whilst can enable multitasking is rather slow and is only avalible on intel x86 systems. Hence only the bare requirements of the TSS will be implemented as if further works require it would then reuire less work to port the operating system to a new system arcitecher.
The TSS stands for the Task State Segment. And due to it being a segment will require it's own entry into the GDT. How ever the gdt entry has some quirks for instance; the descriptorYtpe must be zero to indicate that it is a TSS, conforming is always zero in a TSS, accessed is one for a TSS ReadWrite indicates if the TSS is busy or not instead of the read write permissions, executible instead means is it 32bit or 16bit and the s32bitSeg must be zero.

Also the TSS requires it's own structure which has many values inside. The first of these points to the last tss entry to form a linked list.
The TSS structure is used to store infomation about the machine before a hardware task switch. This will contain things like the registers the flags and even the instruction pointer location.

There are only two required feilds on the TSS structure however this is the ss0 and esp0 feilds. This is because these are the kernel stack segment and kernel stack pointer fields. These are required by the hardware when switching to ring 0. This is because the userland stack may well be different to the kernel stack.

There is an instruction to install a TSS in to the cpu this instruction is the LTR assmbly instruction. This loads the TSS into a special register know as the TSR or task state register.
(OSDEV getting to ring 3)[http://wiki.osdev.org/Getting_to_Ring_3]
(http://www.brokenthorn.com/Resources/OSDev23.html, http://www.brokenthorn.com/Resources/OSDev12.html)
(https://web.archive.org/web/20160326062442/http://jamesmolloy.co.uk/tutorial_html/10.-User%20Mode.html)
(OSDEV Virtual_8086_Mode)[http://wiki.osdev.org/Virtual_8086_Mode] (GCC ASM SYNTAX)[https://www.cs.uaf.edu/2011/fall/cs301/lecture/10_12_asm_c.html]
