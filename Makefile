#Place to out put the compiled files
OUT_DIR?=$(CURDIR)/out
OUT_IMAGE?= "$(OUT_DIR)/image.img"

#location of the compiler to use for building the os
CROSS_COMPILER?= $(HOME)/opt/cross/bin/i686-elf-


#Allow sub-systems access the prams required to build
export OUT_DIR
export OUT_IMAGE
export CROSS_COMPILER
#-------------------------
#A run once solution to setup a new machine to be able to build this system
#-------------------------
envsetup:
	make -C ./src envsetup
#-------------------------
#Build the image but dont run
#-------------------------
build:
	make -C ./src build 
#-------------------------
#Build a new clean version and run in qemu with an auto breakpoint
#-------------------------
qemu-gdb:
	make -C ./src qemu-gdb
#-------------------------
#Build a new clean version and run in qemu without an auto breakpoint
#-------------------------
qemu:
	make -C ./src qemu
#-------------------------
#Easily connects gdb to a running qemu
#-------------------------
gdb-attach:
	make -C ./src gdb-attach
#-------------------------
#Build a new clean version and run in bochs
#-------------------------
bochs:
	make -C ./src bochs

#-------------------------
#Remove all outputed files and make new partitions for each sub-system output
#-------------------------
clean:
	make -C ./src clean
	
